#use wml::debian::translation-check translation="a48c50faef33185d0ae2746854bc99435817bad1"
<define-tag pagetitle>El proyecto Debian lamenta la pérdida de Robert Lemmen, Karl Ramm y Rogério Theodoro de Brito</define-tag>
<define-tag release_date>2021-08-12</define-tag>
#use wml::debian::news

<p>El proyecto Debian ha perdido a varios miembros de su
comunidad en los años 2020 y 2021.</p>

<p>
En junio de 2020 falleció Robert Lemmen tras una grave enfermedad.
Robert asistía regularmente a los encuentros de Debian en Múnich desde
principios de la década de 2000 y ayudaba con los stands locales.
Era desarrollador de Debian desde 2007. Entre otras contribuciones,
empaquetó módulos para Raku (Perl6 en esa época) y ayudó a otros
contribuidores a formar parte del equipo de Raku.
También trabajó duro en el seguimiento de dependencias circulares en Debian.
</p>

<p>
Karl Ramm falleció en junio de 2020 tras complicaciones derivadas de un cáncer de colon con metástasis.
Era desarrollador de Debian desde 2001 y empaquetó varios componentes del
<a href="https://en.wikipedia.org/wiki/Project_Athena">proyecto Athena</a> del MIT.
Era un apasionado de la tecnología y de Debian, 
y siempre se interesaba por ayudar a los demás a encontrar y seguir sus pasiones.
</p>

<p>
En abril de 2021 perdimos a Rogério Theodoro de Brito debido a la pandemia de COVID-19.
Rogério disfrutaba codificando pequeñas herramientas y fue contribuidor a Debian durante más de quince años.
Entre otros proyectos, contribuyó al uso de dispositivos Kurobox/Linkstation en Debian
y fue responsable de la herramienta youtube-dl. También participó, y fue el <q>contacto Debian</q>, en varios proyectos originales.
</p>

<p>El proyecto Debian honra el buen trabajo y la gran dedicación a Debian y al software libre de Robert,
Karl y Rogério. No olvidaremos sus contribuciones, y los
altos estándares de su trabajo seguirán sirviendo de inspiración a
otros.</p>

<h2>Acerca de Debian</h2>
<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>
<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a> o envíe un correo electrónico a
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
