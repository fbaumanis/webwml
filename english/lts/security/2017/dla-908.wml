<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that CHICKEN did not sanitize the size argument when
allocating SRFI-4 vectors, which could lead to segfaults or buffer
overflows with some sizes.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.7.0-1+deb7u2.</p>

<p>We recommend that you upgrade your chicken packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-908.data"
# $Id: $
