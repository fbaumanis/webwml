#use wml::debian::template title="Debian 11 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="2c12526e7c785d665d8d8cae4b75533f682d1365"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Problemas conhecidos</toc-add-entry>
<toc-add-entry name="security">Problemas de segurança</toc-add-entry>

<p>A equipe de segurança do Debian emite atualizações para pacotes na versão
estável (stable), nos quais eles(as) identificaram problemas relacionados à
segurança. Por favor consulte as <a href="$(HOME)/security/">páginas de segurança</a>
para obter informações sobre quaisquer problemas de segurança identificados no
<q>bullseye</q>.</p>

<p>
Se você usa o APT, adicione a seguinte linha ao <tt>/etc/apt/sources.list</tt>
para poder acessar as atualizações de segurança mais recentes:</p>

<pre>
  deb http://security.debian.org/debian-security bullseye-security main contrib non-free
</pre>

<p>Depois disso execute <kbd>apt update</kbd> seguido por
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Lançamentos pontuais</toc-add-entry>

<p>Às vezes, no caso de vários problemas críticos ou atualizações de segurança,
a versão já lançada é atualizada. Geralmente, as atualizações são indicadas
como lançamentos pontuais.</p>

<ul>
  <li>A primeira versão pontual, 11.1, foi lançada em
      <a href="$(HOME)/News/2021/20211009">9 de outubro de 2021</a>.</li>
  <li>A segunda versão pontual, 11.2, foi lançada em
      <a href="$(HOME)/News/2021/20211218">18 de dezembro de 2021</a>.</li>
  <li>A terceira versão pontual, 11.3, foi lançada em
      <a href="$(HOME)/News/2022/20220326">26 de ,arço de 2022</a>.</li></ul>

<ifeq <current_release_bullseye> 11.0 "

<p>Ainda não há lançamentos pontuais para o Debian 11.</p>" "

<p>consulte o <a
href="http://http.us.debian.org/debian/dists/bullseye/ChangeLog">\
ChangeLog</a>
para obter detalhes sobre alterações entre 11 e <current_release_bullseye/>.</p>"/>


<p>As correções na versão estável (stable) lançada geralmente passam por
um longo período de teste antes de serem aceitas no repositório.
No entanto, essas correções estão disponíveis no repositório
<a href="http://ftp.debian.org/debian/dists/bullseye-proposed-updates/">\
dists/bullseye-proposed-updates</a> de qualquer espelho do repositório Debian.</p>

<p>Se você usar o APT para atualizar seus pacotes, poderá instalar as
atualizações propostas adicionando a seguinte linha ao
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# Atualizações propostas para a versão 11
  deb http://deb.debian.org/debian bullseye-proposed-updates main contrib non-free
</pre>

<p>Depois disso, execute <kbd>apt update</kbd> seguido por
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Sistema de instalação</toc-add-entry>

<p>
Para obter informações sobre erratas e atualizações para o sistema de
instalação, consulte a página
<a href="debian-installer/">informações de instalação</a>
</p>
