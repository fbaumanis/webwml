#use wml::debian::translation-check translation="b5442e774e44072d3f8668bab88ee69536ff07cb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans curl, un outil en ligne de
commande pour transférer des données avec une syntaxe d’URL et une bibliothèque
de transfert d’URL, côté client, facile à utiliser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8284">CVE-2020-8284</a>

<p>Quand curl réalise un transfert passif par FTP, il essaie en premier la
commande EPSV et, si elle n’est pas prise en charge, il se replie sur PASV.
Le mode passif est ce que curl utilise par défaut. Une réponse de serveur pour
une commande PASV inclut l’adresse (IPv4) et le numéro de port pour que le client
se reconnecte afin de réaliser le transfert réel des données. C’est la
façon dont le protocole FTP est conçu. Un serveur malveillant peut utiliser la
réponse PASV pour embringuer curl à se reconnecter à une certaine adresse et un
certain port, et de cette façon pouvoir potentiellement extraire des informations
à propos de services qui sont, sauf accord, privés et non divulgués, par
exemple, par la réalisation d’un balayage de ports ou l’extraction de bannières
de service.</p>

<p>La partie adresse IP de la réponse est désormais ignorée par défaut, en
réglant par défaut CURLOPT_FTP_SKIP_PASV_IP à 1L au lieu du réglage précédant 0L.
Cela conduit à l'inconvénient mineur qu’une petite fraction des cas d’utilisation
soient cassés quand le serveur a besoin réellement de se reconnecter à une
adresse IP différente de celle utilisée par le contrôle de connexion et que
celles où CURLOPT_FTP_SKIP_PASV_IP peut être réglé à 0L. La même chose est
valable pour l’outil en ligne de commande qui peut avoir besoin de l’option
--no-ftp-skip-pasv-ip pour empêcher curl d’ignorer l’adresse dans la réponse
du serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8285">CVE-2020-8285</a>

<p>libcurl propose une fonctionnalité de correspondance de joker. Cela permet a
une fonction de rappel (définie avec CURLOPT_CHUNK_BGN_FUNCTION) de renvoyer
des informations à libcurl sur la façon de gérer une entrée particulière
dans un répertoire quand libcurl itère une liste d’entrées disponibles. Quand
cette fonction renvoie CURL_CHUNK_BGN_FUNC_SKIP, pour indiquer à libcurl
d’ignorer ce fichier, la fonction interne dans libcurl s’appelle récursivement
pour gérer la prochaine entrée de répertoire. S’il existe un nombre suffisant
d’entrées de fichier et si le rappel renvoie <q>skip</q> un nombre de fois
suffisant, libcurl débordera de son espace de pile. Le nombre exact dépend
des plateformes, des compilateurs et d’autres facteurs environnementaux. Le
contenu du répertoire distant n’est pas conservé dans la pile, aussi il
semble difficile pour l’attaquant de contrôler précisément quelles données
écrasent la pile. Cependant, cela demeure un vecteur de déni de service, aussi
un utilisateur malveillant qui contrôle le serveur sur lequel l’application
utilisant libcurl sous ces prémisses peut provoquer un plantage.</p>

<p>La fonction interne est réécrite pour utiliser plutôt, et de manière plus
appropriée, une boucle ordinaire au lieu de l’approche récursive. De cette façon
l’utilisation de la pile reste la même quelque soit le nombre de fichiers
sautés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8286">CVE-2020-8286</a>

<p>libcurl propose <q>l’agrafage OCSP</q> à l’aide de l’option
CURLOPT_SSL_VERIFYSTATUS. Lorsqu’elle est active, libcurl vérifie la réponse
OCSP que le serveur renvoie comme part de l’initialisation de connexion TLS. Il
interrompt alors la négociation TLS si quelque chose est faux dans la réponse.
La même fonctionnalité peut être activée avec --cert-status en utilisant l’outil
curl. Dans le cadre de la vérification de réponse OCSP, un client doit vérifier
que la réponse soit effectivement pour le bon certificat. Cette étape n’était pas
réalisée par libcurl lorsque construite ou instruite pour utiliser OpenSSL comme
dorsal TLS. Ce défaut pouvait permettre à un attaquant, qui pouvait peut-être
avoir commis une intrusion dans un serveur TLS, de fournir une réponse OCSP
frauduleuse apparaissant correcte, au lieu de la réponse réelle — comme si le
certificat originel en fait avait été révoqué.</p>

<p>La fonction de vérification de réponse OCSP vérifie désormais que
l’identifiant de certificat soit correct.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 7.52.1-5+deb9u13.</p>

<p>Nous vous recommandons de mettre à jour vos paquets curl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de curl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/curl">https://security-tracker.debian.org/tracker/curl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2500.data"
# $Id: $
