#use wml::debian::translation-check translation="7fbd143ff4054851e34c6474b24d24e272505e9a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de sécurité a été découverte dans lucene-solr, un serveur
d’entreprise pour des recherches.</p>

<p>DataImportHandler (DIH), un module facultatif mais populaire pour extraire des
données de bases de données ou d’autres sources, possédait une fonction
par laquelle la configuration DIH entière pouvait être obtenue avec un paramètre
de requête <q>dataConfig</q>. Le mode de débogage de l’écran de l’administrateur
DIH utilisait cela pour un débogage ou un développement pratique de la
configuration de DIH. Puisque celle-ci pouvait contenir des scripts, ce paramètre
présentait un risque de sécurité. Désormais, l’utilisation de ce paramètre
nécessite de régler la propriété du système Java, <q>enable.dih.dataConfigParam</q>,
à <q>true</q>. Par exemple, cela peut être réalisé avec solr-omcat en ajoutant
-Denable.dih.dataConfigParam=true à JAVA_OPTS dans /etc/default/tomcat8.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 3.6.2+dfsg-10+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lucene-solr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de lucene-solr, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/lucene-solr">https://security-tracker.debian.org/tracker/lucene-solr</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2327.data"
# $Id: $
