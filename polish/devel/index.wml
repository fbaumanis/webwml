#use wml::debian::template title="Kącik Deweloperów Debiana" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="efa5e923d43c06826f515b6647608c5f73fdfc8d"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Informacje na tej stronie, choć publiczne, będą interesowały głównie
deweloperów Debiana.</p>
</aside>

<ul class="toc">
<li><a href="#basic">Podstawy</a></li>
<li><a href="#packaging">Pakiety</a></li>
<li><a href="#workinprogress">Prace w toku</a></li>
<li><a href="#projects">Projekty</a></li>
<li><a href="#miscellaneous">Różne</a></li>
</ul>

<div class="row">

  <!-- left column -->
  <div class="column column-left" id="basic">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="basic">Informacje ogólne</a></h2>
      <p>Lista obecnych deweloperów i opiekunów, jak dołączyć do projektu, odsyłacze do bazy danych deweloperów, konstytucji, procesu głosowania, wydań i architektur.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(HOME)/intro/organization">Organizacja Debiana</a></dt>
        <dd>W projekcie Debian uczestniczy ponad tysiąc ochotników. Ta strona objaśnia strukturę organizacyjną Debiana, zawiera listę zespołów i ich członków oraz adresów kontaktowych.</dd>

        <dt><a href="$(HOME)/intro/people">Ludzie</a></dt>
        <dd>
	Debian jest tworzony wspólnie zarówno przez <a
        href="https://wiki.debian.org/DebianDeveloper">deweloperów Debiana
	(Debian Developers, DD)</a> (którzy są pełnoprawnymi członkami projektu
	Debian), jak i <a
	href="https://wiki.debian.org/DebianMaintainer">opiekunów Debiana
	(Debian Maintainers, DM)</a>. Dostępna jest zarówno <a
	href="https://nm.debian.org/public/people/dd_all/">lista deweloperów Debiana</a>
        jak i <a href="https://nm.debian.org/public/people/dm_all/">lista opiekunów Debiana</a>,
	zawierające więcej informacji na temat tych osób, jak i nazwy pakietów którymi się zajmują.

        Możesz też zobaczyć
        <a href="developers.loc">mapę świata z deweloperami Debiana</a>
	i <a href="https://gallery.debconf.org/">galerię zdjęć</a>
	z różnych wydarzeń związanych z Debianem.
        </dd>

        <dt><a href="join/">Dołączyć do Debiana</a></dt>
        <dd>
        Chcesz pomóc i dołączyć do projektu? Ciągle
        szukamy nowych deweloperów i entuzjastów wolnego oprogramowania z umiejętnościami technicznymi.
        Więcej informacji na powyższej stronie.
        </dd>

        <dt><a href="https://db.debian.org/">Baza danych deweloperów.</a></dt>
        <dd>
        Baza danych zawiera pewne informacje dostępne dla wszystkich, a także
        pewne dostępne tylko dla zalogowanych deweloperów.

        Baza zawiera listę
        <a href="https://db.debian.org/machines.cgi">maszyn projektu</a> i
        <a href="extract_key">kluczy GPG deweloperów</a>.

        Deweloperzy posiadający konto mogą
        <a href="https://db.debian.org/password.html">zmienić swoje hasło</a>
        lub <a href="https://db.debian.org/forward.html">dowiedzieć się, jak
        ustawić przekazywanie poczty</a> dla swojego konta Debiana.

        Jeśli zamierzasz używać maszyn Debiana, upewnij się,
        że przeczytałeś <a href="dmup">Zasady Użycia Maszyn
        Debiana</a>.
        </dd>

        <dt><a href="constitution">Konstytucja</a></dt>
        <dd>
        Ten dokument opisuje strukturę organizacji formalnego podejmowania decyzji w
        projekcie.
        </dd>

        <dt><a href="$(HOME)/vote/">Informacja o głosowaniu</a></dt>
        <dd>
        Jak wybieramy naszych
        liderów i loga, i ogólnie o tym, jak głosujemy.
        </dd>

        <dt><a href="$(HOME)/releases/">Wydania</a></dt>

        <dd>To jest lista obecnych wydań (<a href="$(HOME)/releases/stable/">stabilnego</a>, <a href="$(HOME)/releases/testing/">testowego</a> i <a href="$(HOME)/releases/unstable/">niestabilnego</a>) oraz spis starych wydań i ich nazw kodowych.</dd>

        <dt><a href="$(HOME)/ports/">Różne architektury</a></dt>
        <dd>
        Debian działa na wielu typach komputerów. Ta strona zawiera informacje na temat różnych
        adaptacji Debiana - niektóre na jądrze Linux, inne na FreeBSD, NetBSD i Hurd.
        </dd>
      </dl>
    </div>

  </div>

  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-code fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="packaging">Pakiety</a></h2>
      <p>Odsyłacze do podręcznika polityki i innych powiązanych dokumentów, procedury i inne zasoby dla deweloperów Debiana, oraz przewodnik nowych opiekunów.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(DOC)/debian-policy/">Podręcznik Polityki Debiana</a></dt>
        <dd>
        Ten podręcznik opisuje zasady obowiązujące dystrybucji Debian.
        Zawiera się w tym struktura i zawartość archiwum Debiana, założenia
	co do systemu operacyjnego, jak również wymagania
        techniczne, które każdy pakiet musi spełnić, by być włączonym do
        dystrybucji.

        <p>Krótko: <strong>musisz</strong> to przeczytać.</p>
        </dd>
      </dl>

      <p>Jest kilka dokumentów powiązanych z tymi Zasadami, którymi możesz być zainteresowany,
      takich jak:</p>

      <ul>
        <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Filesystem Hierarchy Standard</a> (FHS)
        <br />FHS definiuje strukturę katalogów i ich zawartość (lokalizację plików);
        zgodność z wersją 3.0 jest wymagana (patrz <a
        href="https://www.debian.org/doc/debian-policy/ch-opersys.html#file-system-hierarchy">rozdział
        9</a> Podręcznika Polityki Debiana).</li>
        <li>Lista pakietów <a href="$(DOC)/packaging-manuals/build-essential">niezbędnych do budowania (build-essential)</a>
        <br />Pakiety typu build-essential są pakietami, które powinieneś mieć
        by skompilować oprogramowanie lub zbudować jakieś pakiety. Nie musisz dołączać ich w lini
        <code>Build-Depends</code> swojego pakietu <a
        href="https://www.debian.org/doc/debian-policy/ch-relationships.html">deklarując
        relacje</a> z innymi pakietami.</li>
        <li><a href="$(DOC)/packaging-manuals/menu-policy/">System Menu</a>
        <br />Debianowa struktura wpisów w menu.
        Sprawdź również <a href="$(DOC)/packaging-manuals/menu.html/">dokumentację
        systemu menu</a>.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Zasady dla Emacsa</a></li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Zasady dla Javy</a></li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Zasady dla Perla</a></li>
	<li><a href="$(DOC)/packaging-manuals/python-policy/">Zasady dla Pythona</a></li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">Specyfikacje Debconf</a></li>
	<li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">Zasady
	aplikacji bazodanowych</a> (szkic)</li>
        <li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Zasady dla Tcl/Tk</a> (szkic)</li>
	<li><a href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Zasady dla Ada</a></li>
      </ul>

      <p>Zapoznaj się również z <a href="https://bugs.debian.org/debian-policy">
      proponowanymi zmianami w Zasadach</a>.</p>

      <dl>
        <dt><a href="$(DOC)/manuals/developers-reference/">Podręcznik deweloperów</a></dt>

        <dd>
        Założeniem tego dokumentu jest przedstawienie ogółu rekomendowanych
        procedur i dostępnych zasobów dla rozwijających Debiana deweloperów.
        Kolejna rzecz, którą <strong>musisz przeczytać</strong>.
        </dd>

        <dt><a href="$(DOC)/manuals/maint-guide/">Przewodnik dla nowych opiekunów</a></dt>

        <dd>
        Ten dokument opisuje budowę pakietu Debiana zwykłym językiem i posiada
	wiele przykładów. Jeśli planujesz zostać deweloperem lub opiekunem, warto od niego zacząć.
        </dd>
      </dl>
    </div>
  </div>
</div>


<h2><a id="workinprogress">Prace w toku: Odsyłacze dla aktywnych deweloperów i opiekunów Debiana</a></h2>

<aside class="light">
  <span class="fas fa-wrench fa-5x"></span>
</aside>

<dl>
  <dt><a href="testing">Dystrybucja testowa ("testing")</a></dt>
  <dd>
  Automatycznie generowana z dystrybucji niestabilnej ("unstable"):
  tu powinien się znaleźć pakiet by mógł pojawić się w następnym wydaniu Debiana.
  </dd>

  <dt><a href="https://bugs.debian.org/release-critical/">Błędy zagrażające
		wydaniu - Relase Critical</a></dt>

  <dd>
  To jest lista błędów, które mogą decydować o usunięciu pakietu
  z dystrybucji &#8222;testowej&#8221;, lub w niektórych przypadkach nawet spowodować
  przestój w wydaniu dystrybucji. Raporty błędów z dotkliwością większą lub
  równą niż &#8222;poważna&#8221; kwalifikują się na listę -- upewnij się, by
  naprawić wszystkie takie błędy w swoich pakietach tak szybko, jak to możliwe.
  </dd>

  <dt><a href="$(HOME)/Bugs/">System śledzenia błędów (BTS)</a></dt>
  <dd>
  Zgłaszanie, dyskutowanie i naprawa błędów. BTS jest przydatny zarówno dla
  użytkowników jak i deweloperów.
  </dd>

  <dt>Informacje na temat pakietów Debiana</dt>
  <dd>
  <a href="https://qa.debian.org/developer.php">Informacja o pakietach</a>
  i strony <a href="https://tracker.debian.org/">śledzenia pakietów</a>
  podają wiele wartościowych informacji dla opiekunów.
  Deweloperzy pragnący śledzić inne pakiety, mogą zapisać się (przez e-mail)
  na usługę wysyłającą kopie wiadomości z systemu śledzenia błędów a także
  powiadomienia o wgraniach i instalacjach. Więcej informacji w <a
  href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">podręczniku
  systemu śledzenia pakietów</a>.
  </dd>

  <dt><a href="wnpp/">Pakiety, które potrzebują pomocy</a></dt>
  <dd>
  Pakiety z Perspektywami i Potrzebujące-Opieki (skrót angielski: WNPP) to lista
  pakietów Debiana, które potrzebują nowych opiekunów, jak również pakietów, które
  jeszcze nie zostały dołączone do Debiana.
  </dd>

  <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">\
      System Incoming</a></dt>
  <dd>
  Nowe pakiety są wgrywane do systemu "Incoming" w wewnętrznych
  serwerach archiwów. Zaakceptowane pakiety są niemal natychmiast
  <a href="https://incoming.debian.org/">dostępne przez HTTP</a>,
  i przekazywane do <a href="$(HOME)/mirror/">serwerów lustrzanych</a> cztery razy dziennie.
  <br />
  <strong>Uwaga</strong>: W związku z naturą systemu Incoming, nie polecamy
  robienia kopii lustrzanych.
  </dd>

  <dt><a href="https://lintian.debian.org/">Raporty Lintiana</a></dt>

  <dd>
  <a href="https://packages.debian.org/unstable/devel/lintian">
  Lintian</a> jest programem, który sprawdza, czy pakiet jest zgodny z
  Zasadami. Powinieneś go użyć przed każdorazowym wgraniem (upload).
  </dd>

  <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">\
      Dystrybucja eksperymentalna</a></dt>
  <dd>
  Dystrybucja <em>eksperymentalna</em> jest używana jako tymczasowe,
	prowizoryczne miejsce dla bardzo eksperymentalnego oprogramowania. Używaj
  <a href="https://packages.debian.org/experimental/">pakietów z dystrybucji
  <em>eksperymentalnej</em></a> tylko, jeśli już wiesz jak używać dystrybucji
  <em>niestabilnej</em>.
  </dd>

  <dt><a href="https://wiki.debian.org/HelpDebian">Wiki Debiana</a></dt>
  <dd>
  Wiki Debiana zawiera informacje dla developerów i innych współpracowników.
  </dd>

</dl>

<h2><a id="projects">Projekty: wewnętrzne grupy i projekty</a></h2>

<aside class="light">
  <span class="fas fa-folder-open fa-5x"></span>
</aside>

<ul>
<li><a href="website/">Strony internetowe Debiana</a></li>
<li><a href="https://ftp-master.debian.org/">Archiwum Debiana</a></li>
<li><a href="$(DOC)/ddp">Projekt Dokumentacji Debiana (DDP)</a></li>
<li>Grupa <a href="https://qa.debian.org/">Zapewnienia Jakości</a></li>
<li><a href="$(HOME)/CD/">Obrazy płyt CD/DVD</a></li>
<li><a href="https://wiki.debian.org/Keysigning">Podpisywanie kluczy</a></li>
<li><a href="https://wiki.debian.org/Keysigning/Coordination">Koordynacja podpisywania kluczy</a></li>
<li><a href="https://wiki.debian.org/DebianIPv6">Projekt Debian IPv6</a></li>
<li><a href="buildd/">Sieć autobuildera</a> i <a href="https://buildd.debian.org/">logi z jej działania</a></li>
<li><a href="$(HOME)/international/l10n/ddtp">Projekt Tłumaczeń Opisów Debiana (DDTP)</a></li>
<li><a href="debian-installer/">Instalator Debiana</a></li>
<li><a href="debian-live/">Debian Live</a></li>
<li><a href="$(HOME)/women/">Debian Women</a></li>
<li><a href="$(HOME)/blends/">Debian Pure Blends</a> (wyselekcjonowane zestawy Debiana)</li>
</ul>


<h2><a id="miscellaneous">Różne odsyłacze</a></h2>

<aside class="light">
  <span class="fas fa-bookmark fa-5x"></span>
</aside>

<ul>
<li><a href="https://debconf-video-team.pages.debian.net/videoplayer/">Nagrania</a> z naszych konferencji.</li>
<li><a href="passwordlessssh">Ustawianie SSH tak, żeby nie pytał o hasło</a>.</li>
<li>Jak <a href="$(HOME)/MailingLists/HOWTO_start_list">poprosić o nową listę dyskusyjną</a>.</li>
<li>Informacje o <a href="$(HOME)/mirror/">tworzeniu serwerów lustrzanych</a> Debiana.</li>
<li>Wykres ilości <a href="https://qa.debian.org/data/bts/graphs/all.png">wszystkich błędów</a>.</li>
<li><a href="https://ftp-master.debian.org/new.html">Nowe pakiety czekające na włączenie do Debiana</a> (kolejka NEW).</li>
<li><a href="https://packages.debian.org/unstable/main/newpkg">Nowe pakiety Debiana z ostatnich 7 dni</a>.</li>
<li><a href="https://ftp-master.debian.org/removals.txt">Pakiety usunięte z Debiana</a>.</li>
</ul>
