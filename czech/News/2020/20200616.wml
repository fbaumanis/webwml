<define-tag pagetitle>Ampere daruje Debianu server Arm64, aby podpořil Arm ekosystém</define-tag>
<define-tag release_date>2020-06-16</define-tag>
#use wml::debian::news 
#use wml::debian::translation-check translation="a3ce03f0ff8939281b7a4da3bb955c91e6857f6f" maintainer="Branislav Makuch"

# Status: [content-frozen]

##
## Translators: 
## - if translating while the file is in publicity-team/announcements repo,
##   please ignore the translation-check header. Publicity team will add it
##   including the correct translation-check header when moving the file 
##   to the web repo
##
## - if translating while the file is in webmaster-team/webwml repo,
##   please use the copypage.pl script to create the page in your language
##   subtree including the correct translation-check header.
##

<p>
<a href="https://amperecomputing.com/">Ampere®</a> 
se stala partnerem Debianu a podpořila hardwarovou infrastrukturu Debianu darováním 
tří výkonných Arm64 serverů.
Tyto servery značky Lenovo ThinkSystem HR330A obsahují CPU čip Ampere eMAG
s 64-bitovým procesorem Arm®v8 speciálně vyvinutým pro cloudové servery,
a jsou vybaveny 256GB RAM, duální 960GB SSD a 256GbE dual port NIC.
</p>

<p>
Darované servery byly umístěny u našeho hostovacího partnera ve Vancouveru
v Kanadě na Univerzitě Britské Kolumbie.
Systémoví Administrátoři Debianu (DSA) je nakonfigurovali, aby na nich běželi sestavovací 
daemoni pro arm64/armhf/armel a tím nahradili daemony na méně výkonných vývojových deskách. 
Na virtuálních strojích s pouhou polovinou alokovaných vCPU, čas na sestavení Arm* balíků byl
na systému Ampere eMAG zkrácen na polovinu. Další výhodou, kterou tento štědrý dar přinesl, 
je možnost pro DSA migrovat některé obecné služby Debianu, jenž běží na naší současné 
infrastruktuře a uvolní tak virtuální stroje pro ostatní týmy Debianu (např: Nepřetržitá 
integrace, Zajištění kvality ad.), které vyžadují přístup k architektuře Arm64.
</p>

<p>
<q>Naše partnerství s Debianem souvisí s naší vývojovou strategií rozšiřovat OpenSource 
komunity, které používají Ampere servery, tím dále budovat ekosystém Arm64 a umožnit 
vývoj nových aplikací,</q> uvedla Mauri Whalen, vicepresidentka oddělení softwarového vývoje 
v Ampere. <q>Debian je dobře spravovaná a respektovaná komunita, a jsme hrdí že s 
nimi můžeme spolupracovat.</q>
</p>

<p>
<q>
Systémoví Administrátoři Debianu jsou vděční Ampere za dar carrier-grade Arm64 serverů.
Mít k dispozici servery s integrovanou správou standardů rozhraní jako Intelligent 
Platform Management Interface (IPMI), a se zárukami hardwaru Lenovo a podporou, 
kterou organizace poskytuje, je přesně to, co si DSA přáli pro architekturu Arm64.
Tyto servery jsou velmi výkonné a velmi dobře vybavené: předpokládáme, že je spolu se 
sestavovacími daemony Arm64 budeme používat pro obecné služby. Mslím, že pro obsluhu 
cloudu budou velmi působivé a jsem nadšený, že Ampere Computing se stalo partnerem 
Debianu.</q> - Luca Filipozzi, Systémový Administrátor Debianu.
</p>

<p>
Debian dokáže nadále plnit svůj cíl a poskytovat svobodný operační systém 
pouze díky práci dobrovolníků, darům věcného vybavení, služeb a finančním darům. 
Velmi si ceníme velkorysosti firmy Ampere.
</p>

<h2>O Ampere Computing</h2>
<p>
Ampere vytváří budoucnost hyperscale cloudů a nejmodernější výpočetní techniky díky
prvnímu procesoru na světě nativně určenému pro cloud. Vytvořením procesoru pro cloud s moderní 
64-bitovou architekturou založenou na Arm serveru, Ampere nabízí svým zákazníkům svobodu 
využít všechny aplikace cloudového computingu. Díky nejmodernější výkonosti cloudu, 
energetické účinnosti a škálovatelnosti, procesory Ampere jsou vytvořeny pro stabilní 
růst cloudu a nejmodernější výpočetní techniky.
</p>

<h2>O Debianu</h2>
<p>
Projekt Debian byl založen v roce 1993 Ianem Murdokem jako skutečně svobodný 
komunitní projekt. Od té doby se projekt rozrostl v jednoho z největších a nejvlivnějších
OpenSource projektů. Tisíce dobrovolníků z celého světa spolupracují, aby vytvářeli 
a spravovali software Debianu. Je dostupný v 70 jazycích a podporuje široký rozsah 
typů počítačů, proto se Debian nazývá <q>univerzálním operačním systémem</q>
</p>


<h2>Kontakty</h2>

<p>Pro další informace prosím navštivte webové stránky Debianu na
<a href="$(HOME)/">https://www.debian.org/</a> nebo pošlete email na
&lt;press@debian.org&gt;.</p>
