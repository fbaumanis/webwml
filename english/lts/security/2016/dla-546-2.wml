<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>DLA 546-1 was incorrectly released before updated clamav packages were
available and there were subsequent issues with the acceptance of the package
(which have since been corrected).  Updates are now available for all
supported LTS architectures.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>Upstream published version 0.99.2.  This update updates wheezy-lts to the
latest upstream release in line with the approach used for other Debian
releases.</p>

<p>The changes are not strictly required for operation, but users of the previous
version in Wheezy may not be able to make use of all current virus signatures
and might get warnings.</p>

<p>For Debian 7 <q>Wheezy</q>, this has been addressed in version
0.99.2+dfsg-0+deb7u2.</p>

<p>Further information about Debian LTS security advisories, how to apply these
updates to your system and frequently asked questions can be found at:
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For Debian 7 <q>Wheezy</q>, these issues have been fixed in clamav version 0.99.2+dfsg-0+deb7u2</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-546-2.data"
# $Id: $
