#use wml::debian::translation-check translation="5d750660e3db7f0c5eccb5dc451d9a0f2e491cbf" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été découverts dans l'accélérateur web
Varnish.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-36740">CVE-2021-36740</a>

<p>Martin Blix Grydeland a découvert que Varnish est vulnérable à des
attaques de dissimulation de requête si le protocole HTTP/2 est activé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23959">CVE-2022-23959</a>

<p>James Kettle a découvert une attaque de dissimulation de requête à
l'encontre de l'implémentation du protocole HTTP/1 dans Varnish.</p></li>

</ul>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 6.1.1-1+deb10u3.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 6.5.1-1+deb11u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets varnish.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de varnish, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/varnish">\
https://security-tracker.debian.org/tracker/varnish</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5088.data"
# $Id: $
