<define-tag pagetitle>Debian 11 <q>Bullseye</q> utgiven</define-tag>
<define-tag release_date>2021-08-14</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="7ce619292bb7825470d6cb611887d68f405623ff" maintainer="Andreas Rönnquist"

<p>Efter 2 år, 1 månad och 9 dagars utveckling, presenterar Debianprojektet
stolt sin nya stabila utgåva 11 (med kodnamnet <q>Bullseye</q>),
som kommer att stödjas de kommande 5 åren tack vare det kombinerade arbetet
mellan 
<a href="https://security-team.debian.org/">Debians säkerhetsgrupp</a>
och gruppen för <a href="https://wiki.debian.org/LTS">Debians långtidsstöd</a>.
</p>

<p>
Debian 11 <q>Bullseye</q> släpps med flera skrivbordsprogram och
skrivbordsmiljöer. Bland dessa inkluderas nu skrivbordsmiljöerna:
</p>
<ul>
<li>Gnome 3.38,</li>
<li>KDE Plasma 5.20,</li>
<li>LXDE 11,</li>
<li>LXQt 0.16,</li>
<li>MATE 1.24,</li>
<li>Xfce 4.16.</li>
</ul>


<p>Denna utgåva innehåller mer än 11294 nya paket vilket ger en total på 59551
paket, utöver en märkbar reduktion av 9519 paket som var märkta
som "obsoleta" och borttagna. 42821 paket har uppdaterats och 5434 paket
förblir oförändrade.
</p>

<p>
<q>Bullseye</q> blir vår första utgåva som tillhandahåller en Linuxkärna med stöd
för filsystemet exFAT vilket används som standard för att montera
exFAT-filsystem. Som en konsekvens av detta krävs det inte längre att
använda filsystem-i-användarrymden-implementationen som tillhandahålls
av paketet exfat-fuse. Verktyg för att skapa och kontrollera
exFAT-filsystem tillhandahålls av paketet exfatprogs.
</p>


<p>
De flesta moderna skrivare kan använda sig av drivrutinsfria utskrifter och
scanning utan behovet av leverantörsspecifika (ofta icke-fria) drivrutiner.

<q>Bullseye</q> kommer med ett nytt paket, ipp-usb, som använder det
leverantörsneutrala IPP-over-USB-protokollet som stöds av många moderna
skrivare. Detta tillåter en USB-enhet att behandlas som en nätverksenhet.
De officiella drivrutinsfria SANE-utskriftsenheterna tillhandahålls av
sane-escl i libsane1, som använder eSCL-protokollet.
</p>

<p>
Systemd i <q>Bullseye</q> aktiverar sin funktionalitet för bestående journaler
som standard, med implicit fallback på flyktig lagring. Detta tillåter
användare som inte är beroende av speciella funktioner att avinstallera
traditionella loggnings-demoner och byta till att endast använda
Systemd-journalen.
</p>

<p>
Debian Med-gruppen har tagit del i kampen mot COVID-19 genom att
paketera mjukvara för forskning rörande viruset på sekvensnivå och
för att bekämpa pandemin med verktygen som används inom epidemiologi.
Gruppens arbete med kvalitetssäkring och Continuous integration är
kritiskt för de konsekvent reproducerbara resultaten som forskningen
kräver.

Debian Med Blend har en bredd av prestandakritiska applikationer som nu
drar fördel från SIMD Everywhere. För att installera paket som paketeras
av Debian Med-gruppen, installera metapaketen som namnges med med-*, som
är vid version 3.6.x.
</p>

<p>
Kinesiska, Japanska, Koreanska och många andra språk har nu en ny
inmatningsmetod Fcitx 5 som är efterträdaren till den populära Fcitx4 i
<q>Buster</q>. Denna nya version har mycket bättre stöd för Wayland-tillägg
(standarddisplayhanteraren).
</p>

<p>
Debian 11 <q>Bullseye</q> inkluderar flera uppdaterade mjukvarupaket
(mer än 72% av alla paket i föregående utgåva), så som:
</p>
<ul>
<li>Apache 2.4.48</li>
<li>BIND DNS Server 9.16</li>
<li>Calligra 3.2</li>
<li>Cryptsetup 2.3</li>
<li>Emacs 27.1</li>
<li>GIMP 2.10.22</li>
<li>GNU Compiler Collection 10.2</li>
<li>GnuPG 2.2.20</li>
<li>Inkscape 1.0.2</li>
<li>LibreOffice 7.0</li>
<li>Linux kernel 5.10 series</li>
<li>MariaDB 10.5</li>
<li>OpenSSH 8.4p1</li>
<li>Perl 5.32</li>
<li>PHP 7.4</li>
<li>PostgreSQL 13</li>
<li>Python 3, 3.9.1</li>
<li>Rustc 1.48</li>
<li>Samba 4.13</li>
<li>Vim 8.2</li>
<li>mer än 59000 andra mjukvarupaket klara att använda, byggda från
mer än 30000 källkodspaket.</li>
</ul>

<p>
Med detta breda urval av paket och dess traditionellt breda
arkitektursstöd, håller sig Debian till sitt mål att vara det
universella operativsystemet. Det passar många olika användningsområden:
från skrivbordssystem till laptops; från utvecklingsservrar till
klustersystem; och för databaser, webb, eller lagringsservrar. På samma
gång säkerställer ytterligare kvalitetssäkringsinsatser, så som automatiska
installations- och uppgraderingstester för alla paket i Debians arkiv, att
<q>Bullseye</q> uppfyller de höga förväntningarna som användare har på en
stabil Debianutgåva.
</p>


<p>
Totalt nio arkitekturer stöds:
64-bitars PC / Intel EM64T / x86-64 (<code>amd64</code>),
32-bitars PC / Intel IA-32 (<code>i386</code>),
64-bitars little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64-bitars IBM S/390 (<code>s390x</code>),
för ARM, <code>armel</code>
och <code>armhf</code> för äldre och senare 32-bitars hårdvara,
plus <code>arm64</code> för den 64-bit <q>AArch64</q>-arkitekturen,
och för MIPS, <code>mipsel</code> (little-endian)-arkitekturerna för 32-bitars hårdvara
och <code>mips64el</code>-arkitekturerna för 64-bitars little-endian-hårdvara.
</p>

<h3>Vill du prova?</h3>
<p>
Om du helt enkelt vill testa Debian 11 <q>Bullseye</q> utan att installera det,
kan du använda en av de tillgängliga <a href="$(HOME)/CD/live/">live-avbildningarna</a> som laddar och kör
det fullständiga operativsystemet i ett skrivskyddat läge i din dators minne.
</p>



<p>
Dessa live-avbildningar tillhandahålls för arkitekturerna <code>amd64</code> och
<code>i386</code> och finns tillgängliga för DVD-skivor, USB-minnen och
nätverksstart-setups. Användaren kan välja mellan olika skrivbordsmiljöer att
testa: GNOME, KDE Plasma, LXDE, LXQt, MATE och Xfce. Debian Live
<q>Bullseye</q> har en standardliveavbildning, så det är även möjligt att
prova ett bassystem av Debian utan något grafiskt användargränssnitt.
</p>

<p>
Om du trivs med operativsystemet har du alternativet att installera från
live-avbildningen till din dators hårddisk. Live-avbildningen inkluderar
den oberoende Calamares-installeraren så väl som den vanliga Debian-installeraren.
Ytterligare information finns tillgänglig i
<a href="$(HOME)/releases/bullseye/releasenotes">versionsfakta</a> samt
<a href="$(HOME)/CD/live/">avsnittet för liveinstallationsavbildningar
på Debians webbplats</a>.
</p>

<p>
För att installera Debian 11 <q>Bullseye</q> direkt på din dators
hårddisk kan välja från en mängd olika installationsmedia så som
Blu-ray, DVD, CD, USB-minne, eller via nätverksanslutning.
Flera skrivbordsmiljöer &mdash; Cinnamon, Gnome, KDE Plasma Desktop and
Applications, LXDE, LXQt, MATE och Xfce &mdash; kan installeras genom dessa
avbildningar.
Utöver detta finns <q>multi-arkitekturs</q>-CDs tillgängliga som ger
stöd för att installera från ett val av arkitekturer från en disk. Eller så
kan du skapa bootbar USB-installationsmedia
(Se <a href="$(HOME)/releases/bullseye/installmanual">Installationsguiden</a>
för ytterligare detaljer).
</p>

<p>
För molnanvändare erbjuder Debian direkt stöd för många av de
mest kända molnlösningarna. Officella Debianavbildningar är enkelt valbara
genom varje marknadsplats för avbildningar. Debian publicerar även
<a href="https://cdimage.debian.org/cdimage/openstack/current/">färdigbyggda
OpenStack-avbildningar</a> för arkitekturerna <code>amd64</code> och
<code>arm64</code>, redo att hämtas och användas i lokala molnlösningar.
</p>

<p>
Debian kan nu installeras i 76 språk, med de flesta av dem tillgängliga både
i text-baserade och grafiska användargränssnitt.
</p>

<p>
Installationsavbildningar kan hämtas redan nu via
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (den rekommenderade metoden),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a>, eller
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; se
<a href="$(HOME)/CD/">Debian på CD</a> för ytterligare information. <q>Bullseye</q> kommer
snart att finnas tillgänglig på fysisk DVD, CD-ROM och Blu-ray Disc från
ett antal <a href="$(HOME)/CD/vendors">försäljare</a>.
</p>


<h3>Uppgradera Debian</h3>
<p>
Uppgraderingar till Debian 11 från den föregående utgåvan, Debian 10
(med kodnamn <q>Buster</q>) hanteras automatiskt med hjälp av
pakethanteringssystemet för de flesta konfigurationer.
Som alltid kan Debiansystem uppgraderas smärtfritt, på plats,
utan något påtvingat stillestånd, men det rekommenderas starkt att läsa
<a href="$(HOME)/releases/bullseye/releasenotes">versionsfakta</a> så
väl som <a href="$(HOME)/releases/bullseye/installmanual">installationsguiden</a>
för möjliga problem, och för detaljerade instruktioner om installationen och
uppgradering. Versionsfakta kommer att förbättras och översättas till 
ytterligare språk under veckorna efter utgåvan.
</p>


<h2>Om Debian</h2>

<p>
Debian är ett fritt operativsystem, utvecklat av
tusentals frivilliga från hela världen som samarbetar via Internet.
Debian-projektets styrkor är dess volontärbas, dess hängivenhet till
Debians Sociala kontrakt och fri mjukvara, och dess åtagande att
tillhandahålla det bästa operativsystemet möjligt. Denna nya
utgåva är ytterligare ett viktigt steg i denna riktning.
</p>


<h2>Kontaktinformation</h2>

<p>
För ytterligare information, var vänlig besök Debians webbsidor på
<a href="$(HOME)/">https://www.debian.org/</a> eller skicka e-post
(på engelska) till &lt;press@debian.org&gt;.
</p>
