<define-tag pagetitle>General Resolution: Voting secrecy</define-tag>
<define-tag status>F</define-tag>
# meanings of the <status> tag:
# P: proposed
# D: discussed
# V: voted on
# F: finished
# O: other (or just write anything else)

#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true"
#use wml::debian::toc
#use wml::debian::votebar


    <h1><pagetitle></h1>
    <toc-display />

# The Tags beginning with v are will become H3 headings and are defined in
# english/template/debian/votebar.wml
# all possible Tags:

# vdate, vtimeline, vnominations, vdebate, vplatforms,
# Proposers
#          vproposer,  vproposera, vproposerb, vproposerc, vproposerd,
#          vproposere, vproposerf
# Seconds
#          vseconds,   vsecondsa, vsecondsb, vsecondsc, vsecondsd, vsecondse,
#          vsecondsf,  vopposition
# vtext, vtextb, vtextc, vtextd, vtexte, vtextf
# vchoices
# vamendments, vamendmentproposer, vamendmentseconds, vamendmenttext
# vproceedings, vmajorityreq, vstatistics, vquorum, vmindiscuss,
# vballot, vforum, voutcome


    <vtimeline />
    <table class="vote">
      <tr>
        <th>Discussion Period:</th>
		<td>2022-02-23</td>
		<td>2022-03-11</td>
      </tr>
          <tr>
            <th>Voting period:</th>
            <td>Sunday 2022-03-13 00:00:00 UTC</td>
            <td>Saturday 2022-03-26 23:59:59 UTC</td>
    </table>

    <vproposera />
    <p>Sam Hartman [<email hartmans@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/02/msg00099.html'>text of proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2022/03/msg00018.html'>amendment</a>]
    </p>
    <vsecondsa />
    <ol>
        <li>Russ Allbery [<email rra@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00101.html'>mail</a>]</li>
        <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00102.html'>mail</a>]</li>
        <li>Bill Blough [<email bblough@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00103.html'>mail</a>]</li>
        <li>Filippo Rusconi [<email lopippo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00104.html'>mail</a>]</li>
        <li>Sean Whitton [<email spwhitton@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00105.html'>mail</a>]</li>
        <li>Pierre-Elliott Bécue [<email peb@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00106.html'>mail</a>]</li>
    </ol>
    <vtexta />
	<h3>Hide identities of Developers casting a particular vote</h3>

<h4>Rationale</h4>

<p>During the vote for GR_2021_002, several developers said they were
uncomfortable voting because under the process at that time, their name
and ballot ranking would be public.
A number of participants in the discussion believe that we would get
election results that more accurately reflect the will of the developers
if we do not make the name associated with a particular vote on the
tally sheet public.
Several people believed that the ranked votes without names attached
would still be valuable public information.</p>

<p>This proposal would treat all elections like DPL elections.
At the same time it relaxes the requirement that the secretary must
conduct a vote via email.  If the requirement for email voting is
removed, then an
<a href='https://lists.debian.org/YhoTRIxtz3AIpO+g@roeckx.be'>experiment</a>
is planned at least with the belenios voting
system. belenios may provide better voter secrecy and an easier
web-based voting system than our current email approach.
If this proposal passes, adopting such an alternative
would require sufficient support in the project but would not require
another constitutional amendment.</p>

<p>This proposal increases our reliance on the secretary's existing power
to decide how votes are conducted.  The lack of an override mechanism
for secretary decisions about how we conduct votes has not been a
problem so far.  However, if we are going to rely on this power to
consider questions like whether the project has sufficient consensus to
adopt an alternate voting mechanism, we need an override mechanism.
So, this proposal introduces such a mechanism.</p>

<h4>Summary of Changes</h4>

<p>1) Do not make the identity of a voter casting a particular vote public.</p>
<p>2) Do not require that votes be conducted by email.</p>
<p>3) Clarify that the developers can replace the secretary at any time.</p>
<p>4) Provide a procedure for overriding the decision of the project
    secretary or their delegate.  Overriding the decision of what super
    majority is required or overriding the determination of election
    outcome requires a 3:1 majority.  The chair of the technical committee
    decides who conducts such votes.</p>
<p>6) Codify that our election system must permit independent verification
    of the outcome given the votes cast and must permit developers to
    confirm their vote is included in the votes cast.</p>

<h4>General Resolution</h4>

<p>The developers resolve to make the changes to the Debian Constitution
embodied in git commit ed88a1e3c1fc367ee89620a73047d84a797c9a1d.
As of February 23, 2022, this commit can be found at
[<a href='https://salsa.debian.org/hartmans/webwml/-/commit/ed88a1e3c1fc367ee89620a73047d84a797c9a1d'>https://salsa.debian.org/hartmans/webwml/-/commit/ed88a1e3c1fc367ee89620a73047d84a797c9a1d</a>
<p>

<p>For convenience a word-diff of the changes is included below.  In case
the diff differs from the commit, the commit governs.</p>

<pre>
@@ -179,9 +179,27 @@ earlier can overrule everyone listed later.&lt;/cite&gt;&lt;/p&gt;
  &lt;/li&gt;

  &lt;li&gt;
    [-&lt;p&gt;In case of-]{+&lt;p&gt;Appoint+} a [-disagreement between-]{+new secretary.
In the normal case ( &sect;7.2) where+} the project
    leader and {+secretary agree on+} the [-incumbent-]{+next+} secretary,
[-appoint a new secretary.&lt;/p&gt;-]{+this power of+}
{+    the developers is not used.&lt;/p&gt;+}
  &lt;/li&gt;
  {+&lt;li&gt;+}
{+    &lt;p&gt;Override a decision of the project secretary or their+}
{+    delegate.&lt;/p&gt;+}

{+    &lt;p&gt;Overriding the determination of what super majority is required+}
{+    for a particular ballot option or overriding the determination of+}
{+    the outcome of an election requires the developers to agree by a+}
{+    3:1 majority.  The determination of the majority required to+}
{+    override a decision of the secretary is not subject to+}
{+    override.&lt;/p&gt;+}

{+    &lt;p&gt;The chair of the technical committee decides who acts as+}
{+    secretary for a general resolution to override a decision of the+}
{+    project secretary or their delegate. If the decision was not made+}
{+    by the chair of the technical committee, the committee chair may+}
{+    themselves act as secretary. The decision of who acts as secretary+}
{+    for such a general resolution is not subject to override.&lt;/p&gt;+}
&lt;/ol&gt;

&lt;h3&gt;4.2. Procedure&lt;/h3&gt;
@@ -228,9 +246,10 @@ earlier can overrule everyone listed later.&lt;/cite&gt;&lt;/p&gt;
    &lt;p&gt;
       Votes are taken by the Project Secretary. Votes, tallies, and
       results are not revealed during the voting period; after the
       vote the Project Secretary lists all the votes {+cast in sufficient
detail that anyone may verify the outcome of the election from the votes cast.
The+}
{+       identity of a developer casting a particular vote is not made+}
{+       public, but developers will be given an option to confirm their vote
is included in the votes+} cast. The voting period is 2 weeks, but may be
varied by up
       to 1 week by the Project Leader.
    &lt;/p&gt;
  &lt;/li&gt;

@@ -247,7 +266,7 @@ earlier can overrule everyone listed later.&lt;/cite&gt;&lt;/p&gt;
  &lt;/li&gt;

  &lt;li&gt;
    &lt;p&gt;Votes are cast[-by email-] in a manner suitable to the Secretary.
    The Secretary determines for each poll whether voters can change
    their votes.&lt;/p&gt;
  &lt;/li&gt;
@@ -371,8 +390,7 @@ earlier can overrule everyone listed later.&lt;/cite&gt;&lt;/p&gt;
  necessary.&lt;/li&gt;

  &lt;li&gt;The next two weeks are the polling period during which
  Developers may cast their votes. [-Votes in leadership elections are-]
[-  kept secret, even after the election is finished.&lt;/li&gt;-]{+&lt;/li&gt;+}

  &lt;li&gt;The options on the ballot will be those candidates who have
  nominated themselves and have not yet withdrawn, plus None Of The
</pre>

    <vproposerb />
    <p>Judit Foglszinger [<email urbec@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/02/msg00108.html'>text of proposal</a>]
	[<a href='https://lists.debian.org/debian-vote/2022/03/msg00082.html'>update</a>]
    </p>
    <vsecondsb />
    <ol>
        <li>Scott Kitterman [<email kitterman@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00110.html'>mail</a>]</li>
        <li>Felix Lechner [<email lechner@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00116.html'>mail</a>]</li>
        <li>Louis-Philippe Véronneau [<email pollo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00123.html'>mail</a>]</li>
        <li>Mathias Behrle [<email mbehrle@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00124.html'>mail</a>]</li>
        <li>Tiago Bortoletto Vaz [<email tiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00125.html'>mail</a>]</li>
    </ol>
    <vtextb />
<h3>Hide identities of Developers casting a particular vote and allow verification</h3>
<h4>Rationale</h4>

<p>Give the opportunity to vote for secret voting without needing to
additionally vote for unrelated/only slightly related
constitution changes;
for example for the change of mode of voting
from email to something not defined.</p>

<p>As it was mentioned in the discussion,
there might be no consensus on which options are direcly related -
This option regards the need to allow verification (6))
as directly related to secret votes, because otherwise
they would become completely unverifiable.</p>

<h4>Summary of Changes</h4>

<p>1) Do not make the identity of a voter casting a particular vote
   public.</p>

<p>6) Codify that our election system must permit independent verification
   of the outcome given the votes cast and must permit developers to
   confirm their vote is included in the votes cast.</p>


<pre>
&lt;h3&gt;4.2. Procedure&lt;/h3&gt;
@@ -228,9 +246,10 @@ earlier can overrule everyone listed later.&lt;/cite&gt;&lt;/p&gt;
    &lt;p&gt;
       Votes are taken by the Project Secretary. Votes, tallies, and
       results are not revealed during the voting period; after the
       vote the Project Secretary lists all the votes {+cast in sufficient detail that anyone may verify the outcome of the election from the votes cast. The+}
{+       identity of a developer casting a particular vote is not made+}
{+       public, but developers will be given an option to confirm that their vote is included in the votes+} cast.

@@ -371,8 +390,7 @@ earlier can overrule everyone listed later.&lt;/cite&gt;&lt;/p&gt;
  necessary.&lt;/li&gt;

  &lt;li&gt;The next two weeks are the polling period during which
  Developers may cast their votes. [-Votes in leadership elections are-]
[-  kept secret, even after the election is finished.&lt;/li&gt;-]{+&lt;/li&gt;+}
</pre>

    <vproposerc />
    <p>Holger Levsen [<email holger@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/03/msg00021.html'>text of proposal</a>]
    </p>
    <vsecondsc />
    <ol>
        <li>Mattia Rizzolo [<email mattia@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00022.html'>mail</a>]</li>
        <li>Philip Hands [<email philh@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00030.html'>mail</a>]</li>
        <li>Mathias Behrle [<email mbehrle@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00031.html'>mail</a>]</li>
        <li>Santiago Ruano Rincón [<email santiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00033.html'>mail</a>]</li>
        <li>Gunnar Wolf [<email gwolf@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00037.html'>mail</a>]</li>
        <li>Sven Bartscher [<email kritzefitz@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00058.html'>mail</a>]</li>
    </ol>
    <vtextc />
<h3>Reaffirm public voting</h3>

<p>Since we can either have secret and intransparent voting, or we can have
open and transparent voting, the project resolves to leave our voting
system as it is.</p>

<h4>Rationale</h4>

<p>The GR proposal for secret voting is silent on implenentation details,
probably because secret and transparent voting is, well, impossible to
achieve fully, so this GR is bound to a similar fate as the 'publish
debian-private' vote, which was voted for and then was never implemented.</p>

<p>A voting system which is transparent only to some, is undemocratic and
will lead to few people in the know, which is diagonal to Debians goals
of openness and transparency.</p>

<p>And then, early 2022 is not the time for rushed changes like this, which
is also why I explicitly want to see "keep the status quo" on the ballot,
and not only as "NOTA", but as a real option.</p>

    <vquorum />
     <p>
        With the current list of <a href="vote_001_quorum.log">voting
          developers</a>, we have:
     </p>
    <pre>
#include 'vote_001_quorum.txt'
    </pre>
#include 'vote_001_quorum.src'


    <vstatistics />
    <p>
	For this GR, like always,
#                <a href="https://vote.debian.org/~secretary/gr_vote_secrecy/">statistics</a>
               <a href="suppl_001_stats">statistics</a>
             will be gathered about ballots received and
             acknowledgements sent periodically during the voting
             period.
               Additionally, the list of <a
             href="vote_001_voters.txt">voters</a> will be
             recorded. Also, the <a href="vote_001_tally.txt">tally
             sheet</a> will also be made available to be viewed.
         </p>

    <vmajorityreq />
    <p>
      Proposal 1 and 2 need a 3:1 super majority
    </p>
#include 'vote_001_majority.src'

    <voutcome />
#include 'vote_001_results.src'

    <hrline />
      <address>
        <a href="mailto:secretary@debian.org">Debian Project Secretary</a>
      </address>

