<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in openssl1.0, a Secure Sockets Layer library.
The issue is related to read buffer overruns while processing ASN.1
strings.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1.0.2u-1~deb9u6.</p>

<p>We recommend that you upgrade your openssl1.0 packages.</p>

<p>For the detailed security status of openssl1.0 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl1.0">https://security-tracker.debian.org/tracker/openssl1.0</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2774.data"
# $Id: $
