<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Different flaws have been found in leptonlib, an image processing
library. </p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7186">CVE-2018-7186</a>

    <p>Leptonica did not limit the number of characters in a %s format
    argument to fscanf or sscanf, that made it possible to remote
    attackers to cause a denial of service (stack-based buffer overflow)
    or possibly have unspecified other impact via a long string.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7440">CVE-2018-7440</a>

    <p>The gplotMakeOutput function allowed command injection via a
    $(command) approach in the gplot rootname argument. This issue
    existed because of an incomplete fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-3836">CVE-2018-3836</a>.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.69-3.1+deb7u2.</p>

<p>We recommend that you upgrade your leptonlib packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1302.data"
# $Id: $
