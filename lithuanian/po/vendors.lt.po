msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2009-03-13 16:00+0300\n"
"Last-Translator: Aleksandr Charkov <alcharkov@gmail.com>\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Pardavėjas"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Leidžia įnašus"

#: ../../english/CD/vendors/vendors.CD.def:16
#, fuzzy
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Architektūros"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Tarptautinės Siuntos"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Kontaktas"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Pardavėjo Tinklalapis:"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "puslapis"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "el. paštas"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "Tik Europoje"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "Į kelias šalis"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "išeities tekstas"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "ir"

#~ msgid "updated monthly"
#~ msgstr "Atnaujinama kas menes"

#~ msgid "updated twice weekly"
#~ msgstr "Atnaujinama dukart per savaitę"

#~ msgid "updated weekly"
#~ msgstr "Atnaujinama kas savaitę"

#~ msgid "reseller"
#~ msgstr "perpardavinėtojas"

#~ msgid "reseller of $var"
#~ msgstr "$var perpardavinėtojas "

#~ msgid "Custom Release"
#~ msgstr "Papildoma išleista versiją"

#~ msgid "vendor additions"
#~ msgstr "pardavėjų priedai"

#~ msgid "contrib included"
#~ msgstr "contrib įjungta"

#~ msgid "non-free included"
#~ msgstr "non-free įjungta"

#~ msgid "non-US included"
#~ msgstr "non-US įjungta"

#~ msgid "Multiple Distribution"
#~ msgstr "sudėtinis distributyvas"

#~ msgid "Vendor Release"
#~ msgstr "Gamintojo Išleista Versiją"

#~ msgid "Development Snapshot"
#~ msgstr "Kūrimo momentinė nuotrauka"

#~ msgid "Official DVD"
#~ msgstr "Oficialus DVD"

#~ msgid "Official CD"
#~ msgstr "Oficialus CD"

#~ msgid "Architectures:"
#~ msgstr "Architektūros"

#~ msgid "DVD Type:"
#~ msgstr "DVD Tipas:"

#~ msgid "CD Type:"
#~ msgstr "CD Tipas:"

#~ msgid "email:"
#~ msgstr "el. paštas:"

#~ msgid "Ship International:"
#~ msgstr "Tarptautinės Siuntos:"

#~ msgid "Country:"
#~ msgstr "Šalis:"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Leidžiama įnešti į Debian:"

#~ msgid "URL for Debian Page:"
#~ msgstr "URL Debian Tinklapiui:"

#~ msgid "Vendor:"
#~ msgstr "Pardavėjas:"
