<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in Subversion, a version control
system. The Common Vulnerabilities and Exposures project identifies the
following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11782">CVE-2018-11782</a>

    <p>Ace Olszowka reported that the Subversion's svnserve server process
    may exit when a well-formed read-only request produces a particular
    answer, leading to a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0203">CVE-2019-0203</a>

    <p>Tomas Bortoli reported that the Subversion's svnserve server process
    may exit when a client sends certain sequences of protocol commands.
    If the server is configured with anonymous access enabled this could
    lead to a remote unauthenticated denial of service.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.8.10-6+deb8u7.</p>

<p>We recommend that you upgrade your subversion packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1903.data"
# $Id: $
