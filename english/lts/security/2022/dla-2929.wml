<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One issue have been discovered in ujson: ultra fast JSON encoder and decoder for Python.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45958">CVE-2021-45958</a>

    <p>Stack-based buffer overflow in Buffer_AppendIndentUnchecked (called from encode) has
    been detected. Exploitation can, for example, use a large amount of indentation.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1.35-1+deb9u1.</p>

<p>We recommend that you upgrade your ujson packages.</p>

<p>For the detailed security status of ujson please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ujson">https://security-tracker.debian.org/tracker/ujson</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2929.data"
# $Id: $
