#use wml::debian::translation-check translation="17d4c242ab055d2283825097058cc25beb58ea60" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour est désormais disponible pour toutes les architectures
prises en charge. Pour information, voici le texte de l’annonce originale.</p>

<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8839">CVE-2015-8839</a>

<p>Une situation de compétition a été découverte dans l’implémentation du
système de fichiers ext4. Un utilisateur local pourrait exploiter cela pour
provoquer un déni de service (corruption du système de fichiers).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14610">CVE-2018-14610</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-14611">CVE-2018-14611</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-14612">CVE-2018-14612</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-14613">CVE-2018-14613</a>

<p>Wen Xu du SSLab à Gatech a signalé que des volumes Btrfs contrefaits pourraient
déclencher un plantage (Oops) ou un accès en mémoire hors limites. Un attaquant
capable de monter un tel volume pourrait utiliser cela pour provoquer un déni de
service ou, éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5108">CVE-2019-5108</a>

<p>Mitchell Frank de Cisco a découvert que quand la pile IEEE 802.11 (WiFi) était
utilisée dans le mode accès sans fil (AP) avec itinérance, elle pouvait déclencher
l’itinérance pour une nouvelle station associée avant que la station ne soit
authentifiée. Un attaquant dans la portée du point d’accès pourrait utiliser
cela pour provoquer un déni de service, soit en remplissant une table de
commutateur ou en redirigeant ailleurs le trafic d’autres stations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19319">CVE-2019-19319</a>

<p>Jungyeon a découvert qu’un système de fichiers contrefait peut provoquer que
l’implémentation d’ext4 alloue ou désalloue des blocs de journal. Un utilisateur
autorisé à monter des systèmes de fichiers pourrait utiliser cela pour provoquer
un déni de service (plantage), ou, éventuellement, pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19447">CVE-2019-19447</a>

<p>Il a été découvert que le pilote du système de fichiers ext4 ne gérait pas
de manière sécurisée la dissociation d’un inœud qui, à cause de la corruption du
système de fichiers, a un total de lien égal à zéro. Un attaquant capable de
monter des volumes ext4 arbitraires pourrait utiliser cela pour provoquer un
déni de service (corruption de mémoire ou plantage) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19768">CVE-2019-19768</a>

<p>Tristan Madani a signalé une situation de compétition dans l’utilitaire
blktrace de débogage qui pourrait aboutir dans une utilisation de mémoire après
libération. Un utilisateur local, capable de déclencher l’enlèvement de
périphériques blocs, pourrait éventuellement utiliser cela afin de provoquer un
déni de service (plantage) ou pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20636">CVE-2019-20636</a>

<p>L’outil syzbot a trouvé que le sous-système de saisie ne vérifie pas
complètement les modifications de codes de touche, ce qui pourrait aboutir à une
écriture de tas hors limites. Un utilisateur local, autorisé à accéder au nœud de
périphérique pour un périphérique de saisie ou vidéo, pourrait éventuellement utiliser
cela pour provoquer un déni de service (plantage ou corruption de mémoire) ou
pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0009">CVE-2020-0009</a>

<p>Jann Horn a signalé que le pilote ashmem Android n’empêchait pas que des
fichiers en lecture seule soient mappés en mémoire puis remappés en lecture
et écriture. Toutefois, les pilotes Android ne sont pas autorisés dans les
configurations noyau de Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0543">CVE-2020-0543</a>

<p>Des chercheurs à VU Amsterdam ont découvert que sur quelques CPU d’Intel
gérant les instructions RDRAND et RDSEED, une partie de la valeur aléatoire
générée par ces instructions peut être utilisée dans une exécution spéculative
ultérieure sur n’importe quel cœur du même CPU physique. Selon la façon dont
ces instructions sont utilisées par les applications, un utilisateur local ou une VM
cliente pourrait utiliser cela pour obtenir des informations sensibles telles
que des clés de chiffrement d’autres utilisateurs ou d’autres VM.</p>

<p>Cette vulnérabilité peut être atténuée par une mise à jour du microcode, soit
comme partie du micrologiciel du système (BIOS) ou à l’aide du paquet
intel-microcode de la section non-free de l’archive de Debian. Cette mise
à jour du noyau fournit seulement le signalement de la vulnérabilité et la
possibilité de désactiver cette atténuation si elle n’est pas nécessaire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1749">CVE-2020-1749</a>

<p>Xiumei Mu a signalé que certains protocoles de réseau pouvant être exécutés
au-dessus de IPv6 pourraient contourner la couche de transformation (XFRM)
utilisée par IPsec, IPcomp/IPcomp6, IPIP et IPv6 Mobility. Cela pourrait aboutir
à une divulgation d’informations à travers le réseau, puisqu’elles pourraient ne
pas être chiffrées ou acheminées selon la politique du système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-2732">CVE-2020-2732</a>

<p>Paulo Bonzini a découvert que l’implémentation de KVM pour les processeurs
d’Intel ne gérait pas correctement l’émulation d’instructions pour les clients
L2 lorsque la virtualisation imbriquée est activée. Cela pourrait permettre
à un client L2 de provoquer une élévation des privilèges, un déni de service ou une
fuite d'informations dans le client L1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8647">CVE-2020-8647</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2020-8649">CVE-2020-8649</a>

<p>L’outil Hulk Robot a trouvé un accès MMIO hors limites potentiel dans le
pilote vgacon. Un utilisateur local autorisé à accéder à un terminal virtuel
(/dev/tty1, etc.) sur le système utilisant le pilote vgacon pourrait utiliser
cela pour provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8648">CVE-2020-8648</a>

<p>L’ol syzbot à trouvé une situation de compétition dans le pilote de terminal
virtuel qui pourrait aboutir à une utilisation de mémoire après libération. Un
utilisateur local autorisé à accéder à un terminal pourrait utiliser cela pour
provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9383">CVE-2020-9383</a>

<p>Jordy Zomer a signalé une vérification défectueuse d’intervalle dans le
pilote de disquette qui pourrait conduire à un accès statique hors limites. Un
utilisateur local autorisé à accéder à un pilote de disquette pourrait utiliser
cela pour provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10690">CVE-2020-10690</a>

<p>Il a été découvert que le sous-système d’horloge matérielle PTP ne gérait pas
correctement la durée de vie de périphérique. L’enlèvement d’une horloge
matérielle PTP du système pendant qu’un processus d’utilisateur l’emploie
pourrait conduire à une utilisation de mémoire après libération. L’impact de
sécurité est nébuleux.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10751">CVE-2020-10751</a>

<p>Dmitry Vyukov a signalé que le sous-système SELinux ne gérait pas
correctement plusieurs messages. Cela pourrait permettre à un attaquant
privilégié de contourner les restrictions netlink SELinux.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10942">CVE-2020-10942</a>

<p>Il a été découvert que le pilote vhost_net driver ne validait pas
correctement le type de sockets définis comme dorsaux. Un utilisateur local
autorisé à accéder à /dev/vhost-net pourrait utiliser cela pour provoquer une
corruption de pile à l’aide d’appels système contrefaits, aboutissant à un déni
de service (plantage) ou, éventuellement, à une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11494">CVE-2020-11494</a>

<p>Il a été découvert que le pilote de réseau slcan (serial line CAN)
n’initialisait pas entièrement les en-têtes CAN pour les paquets reçus,
conduisant à une fuite d'informations du noyau vers l’espace utilisateur ou
à travers le réseau CAN.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11565">CVE-2020-11565</a>

<p>Entropy Moe a signalé que le système de fichiers mémoire partagé (tmpfs) ne
gérait pas une option de montage <q>mpol</q> indiquant une liste de nœuds vide,
conduisant à une écriture basée sur la pile hors limites. Si les espaces de
nommage utilisateur sont activés, un utilisateur local pourrait utiliser cela
pour provoquer un déni de service (plantage) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11608">CVE-2020-11608</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2020-11609">CVE-2020-11609</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2020-11668">CVE-2020-11668</a>

<p>Il a été découvert que les pilotes de média ov519, stv06xx et xirlink_cit ne
ne validaient pas correctement les descripteurs de périphérique USB. Un
utilisateur physiquement présent avec un périphérique USB spécialement construit
pouvait utiliser cela pour provoquer un déni de service (plantage) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12114">CVE-2020-12114</a>

<p>Piotr Krysiuk a découvert une situation de compétition entre les opérations
umount et pivot_root dans le cœur du système de fichiers (vfs). Un utilisateur
local avec la capacité CAP_SYS_ADMIN dans n’importe quel espace de nommage
d’utilisateur pourrait utiliser cela pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12464">CVE-2020-12464</a>

<p>Kyungtae Kim a signalé une situation de compétition dans le cœur USB qui
pourrait aboutir à une utilisation de mémoire après libération. L’exploitation
de cela n’est pas évidente, mais pourrait aboutir à un déni de service
(plantage ou corruption de mémoire) ou à une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12652">CVE-2020-12652</a>

<p>Tom Hatskevich a signalé un bogue dans les pilotes de stockage mptfusion.
Un gestionnaire ioctl récupère deux fois un paramètre de la mémoire de
l’utilisateur, créant une situation de compétition qui pourrait aboutir à un
verrouillage de structures de données internes. Un utilisateur local autorisé
à accéder à /dev/mptctl pourrait utiliser cela pour provoquer un déni de service
(plantage ou corruption de mémoire) ou pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12653">CVE-2020-12653</a>

<p>Il a été découvert que le pilote wifi mwifiex ne validait pas suffisamment
les requêtes de balayage, aboutissant à un dépassement potentiel de tampon de
tas. Un utilisateur local avec la capacité CAP_NET_ADMIN pourrait utiliser cela
pour provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12654">CVE-2020-12654</a>

<p>Il a été découvert que le pilote wifi mwifiex ne validait pas suffisamment les
paramètres WMM reçus d’un point d’accès (AP), aboutissant à un dépassement
potentiel de tampon de tas. Un AP malveillant pourrait utiliser cela pour
provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour exécuter du code sur un système vulnérable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12769">CVE-2020-12769</a>

<p>Il a été découvert que le pilote d’hôte SPI spi-dw ne sérialisait pas
correctement l’accès à son état interne. L’impact de sécurité est nébuleux et
ce pilote n’est pas inclus dans les paquets binaires de Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12770">CVE-2020-12770</a>

<p>Il a été découvert que le pilote sg (SCSI générique) ne publiait pas
correctement ses ressources internes dans un cas d’erreur particulier. Un
utilisateur local autorisé à accéder à un périphérique sg pourrait
éventuellement utiliser cela pour provoquer un déni de service (épuisement de
ressources).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12826">CVE-2020-12826</a>

<p>Adam Zabrocki a signalé une insuffisance dans la vérification de signal du
sous-système de permissions. Un processus parent peut choisir un signal
arbitraire pour un processus enfant à envoyer lorsque celui-ci quitte, mais si
le parent a exécuté un nouveau programme, alors le signal par défaut SIGCHLD est
envoyé. Un utilisateur local autorisé à exécuter un programme pendant plusieurs
jours pourrait contourner cette vérification, exécuter un programme setuid et
puis lui envoyer un signal arbitraire. Selon les programmes setuid installés,
cela pourrait avoir un impact de sécurité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13143">CVE-2020-13143</a>

<p>Kyungtae Kim a signalé une écriture potentielle de tas hors limites dans le
sous-système gadget USB. Un utilisateur local autorisé à écrire sur le système
de fichiers de configuration gadget pourrait utiliser cela pour provoquer un
déni de service (plantage ou corruption de mémoire) ou, éventuellement, pour
une élévation des privilèges.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.16.84-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2241-2.data"
# $Id: $
