#use wml::debian::template title="Documentação"
#use wml::debian::translation-check translation="fca004f92b97a053bc89c121827943f1eae0531b"

<p>Uma parte importante de qualquer sistema operacional é a documentação, os
manuais técnicos que descrevem o uso e funcionamento dos programas. Como parte
de seus valores para criar um sistema operacional livre de alta qualidade, o
Projeto Debian se esforça para fornecer a todos os(as) seus(suas)
usuários(as) uma documentação apropriada e facilmente acessível.</p>

<h2>Início rápido</h2>

<p>Se você é <em>novato(a)</em> no Debian, nós recomendamos que comece sua
leitura por:</p>

<ul>
  <li><a href="$(HOME)/releases/stable/installmanual">Guia de instalação</a></li>
  <li><a href="manuals/debian-faq/">FAQ do Debian GNU/Linux</a></li>
</ul>

<p>Tenha estes à mão quanto fizer sua primeira instalação Debian, eles
provavelmente responderão a várias questões e te ajudarão a trabalhar com seu
novo sistema Debian. Posteriormente você poderá ir para:</p>

<ul>
  <li><a href="manuals/debian-handbook/">O Manual do(a) administrador(a) Debian</a>, o manual completo do(a) usuário(a)</li>
  <li><a href="manuals/debian-reference/">Referência Debian</a>,
      um guia do(a) usuário(a) conciso com foco em linha de comando shell</li>
  <li><a href="$(HOME)/releases/stable/releasenotes">Notas de lançamento</a>,
      para pessoas que estão fazendo uma atualização</li>
  <li><a href="https://wiki.debian.org/">Wiki do Debian</a>, uma boa fonte de
      informação para os(as) recém-chegados(as)</li>
</ul>


<p>Finalmente, certifique-se de imprimir e ter em mãos o <a
href="https://www.debian.org/doc/manuals/refcard/refcard">cartão de referência Debian
GNU/Linux</a>, uma listagem dos mais importantes comandos para sistemas Debian.</p>

<p>Há uma quantia razoável de outras documentações listadas abaixo.</p>

<h2>Tipos de documentação</h2>

<p>A maior parte da documentação incluída no Debian foi escrita para o
GNU/Linux em geral. Há também alguma documentação escrita especificamente
para o Debian. Esses documentos estão nas categorias básicas abaixo:</p>

<ul>
  <li><a href="#manuals">Manuais</a></li>
  <li><a href="#howtos">HOWTOs</a></li>
  <li><a href="#faqs">FAQs</a></li>
  <li><a href="#other">Outros documentos mais curtos</a></li>
</ul>

<h3 id="manuals">Manuais</h3>

<p>Os manuais lembram livros, porque eles descrevem tópicos importantes
de forma compreensível.</p>

<p>Muitos dos manuais listados aqui estão disponíveis tanto on-line quanto
em pacotes Debian; na verdade, a maioria dos manuais do site web foram
extraídos de seus respectivos pacotes Debian. Escolha um manual abaixo para
saber seu nome de pacote e/ou links para as versões on-line.</p>

<h3>Manuais específicos do Debian</h3>

<div class="line">
  <div class="item col50">

    <h4><a href="user-manuals">Manuais dos(as) usuários(as)</a></h4>
    <ul>
      <li><a href="user-manuals#faq">FAQ do Debian GNU/Linux</a></li>
      <li><a href="user-manuals#install">Guia de instalação Debian</a></li>
      <li><a href="user-manuals#relnotes">Notas de lançamento do Debian</a></li>
      <li><a href="user-manuals#refcard">Cartão de referência Debian</a></li>
      <li><a href="user-manuals#debian-handbook">O manual do(a) administrador(a) Debian</a></li>
      <li><a href="user-manuals#quick-reference">Referência Debian</a></li>
      <li><a href="user-manuals#securing">Manual de segurança Debian</a></li>
      <li><a href="user-manuals#aptitude">Manual do(a) usuário(a) do aptitude</a></li>
      <li><a href="user-manuals#apt-guide">Manual do(a) usuário(a) do APT</a></li>
      <li><a href="user-manuals#apt-offline">Usando o APT offline</a></li>
      <li><a href="user-manuals#java-faq">FAQ do Debian GNU/Linux e Java</a></li>
      <li><a href="user-manuals#hamradio-maintguide">Manual do(a) mantenedor(a) dos pacotes de Rádio Amador Debian</a></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h4><a href="devel-manuals">Manuais dos(as) desenvolvedores(as)</a></h4>
    <ul>
      <li><a href="devel-manuals#policy">Manual de Políticas Debian</a></li>
      <li><a href="devel-manuals#devref">Referência dos(as) desenvolvedores(as) Debian</a></li>
      <li><a href="devel-manuals#debmake-doc">Guia dos(as) mantenedores(as) Debian</a></li>
      <li><a href="devel-manuals#packaging-tutorial">Introdução ao empacotamento Debian</a></li>
      <li><a href="devel-manuals#menu">Sistema de menu Debian</a></li>
      <li><a href="devel-manuals#d-i-internals">Instaladores internos do Debian</a></li>
      <li><a href="devel-manuals#dbconfig-common">Guia para mantenedores(as) de pacotes que usam banco de dados</a></li>
      <li><a href="devel-manuals#dbapp-policy">Políticas para pacotes que usam banco de dados</a></li>
    </ul>

    <h4><a href="misc-manuals">Manuais diversos</a></h4>
    <ul>
      <li><a href="misc-manuals#history">História do Projeto Debian</a></li>
    </ul>

  </div>


</div>

<p class="clr">A lista completa de manuais do Debian e outros documentos pode ser
encontrada nas páginas do <a href="ddp">projeto de documentação Debian</a>.</p>

<p>Há também vários manuais escritos para o Debian GNU/Linux orientados para
os(as) usuários(as), disponíveis em forma de <a href="books">livros impressos</a>.</p>

<h3 id="howtos">HOWTOs</h3>

<p>Os <a href="https://tldp.org/docs.html#howto">documentos HOWTO</a>,
como o próprio nome em inglês diz, descrevem <em>como fazer</em> algo,
e eles normalmente cobrem um assunto mais específico.</p>


<h3 id="faqs">FAQs</h3>

<p>FAQ significa <em>frequently asked questions</em> (perguntas realizadas
frequentemente). Um FAQ é um documento que responde essas perguntas.</p>

<p>Perguntas relacionadas especificamente ao Debian são respondidas
no <a href="manuals/debian-faq/">FAQ do Debian</a>.
Também há em separado um <a href="../CD/faq/">FAQ sobre as imagens de
CD/DVD do Debian</a>.</p>


<h3 id="other">Outros documentos menores</h3>

<p>Os documentos seguintes incluem instruções mais rápidas e pequenas:</p>

<dl>

  <dt><strong><a href="https://tldp.org/docs.html#man">Páginas de manual</a></strong></dt>
    <dd>Tradicionalmente, todos os programas Unix são documentados com
    <em>páginas de manual</em>, manuais de referência estão disponíveis
        através do comando <tt>man</tt>. Eles normalmente não são para
        novatos(as). Você pode buscar e ler as páginas de manual disponíveis no
        Debian em <a href="https://manpages.debian.org/cgi-bin/man.cgi">https://manpages.debian.org/</a>.
    </dd>

  <dt><strong><a href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">Arquivos info</a></strong></dt>
    <dd>Muitos programas GNU são documentados através de <em>arquivos info</em>
        ao invés de páginas de manual. Esses arquivos incluem informações
        detalhadas sobre o programa, opções e exemplos de uso, e estão
        disponíveis através do comando
        <tt>info</tt>.
    </dd>

  <dt><strong>Arquivos LEIA-ME (<q>README</q>) variados</strong></dt>
    <dd>Os arquivos <em>leia-me</em> (<q>readme</q>) também são comuns &mdash; eles são arquivos
        de texto simples que descrevem um item, normalmente um pacote. Você pode
        encontrar muitos deles nos subdiretórios em <tt>/usr/share/doc/</tt> no
        seu sistema Debian. Cada pacote de software tem um subdiretório com seus
        próprios arquivos leia-me (<q>readme</q>), e pode também incluir exemplos de
        configuração. Note que, em programas maiores, a documentação é
        fornecida geralmente através de um pacote separado (com o mesmo nome do
        pacote original, mas terminado em <em>-doc</em>).
    </dd>

  <dt><strong>Cartões de referência rápida</strong></dt>
    <dd>
        <p>Cartões de referência rápida são resumos bem curtos de um
        (sub)sistema específico. Geralmente, um cartão de referência fornece os
        comandos mais usados em um único documento. Algumas coleções e
        principais cartões de referência estão incluídos no:</p>
        <dl>
          <dt><a href="https://www.debian.org/doc/manuals/refcard/refcard">Cartão
          de referência Debian GNU/Linux</a></dt>
      <dd>Esse cartão, que pode ser impresso em uma única folha de papel,
          fornece uma lista dos comandos mais importantes e é uma boa
          referência para novos(as) usuários(as) do Debian que querem se
          familiarizar com eles. É necessário, pelo menos, um conhecimento
          básico de computadores, arquivos, diretórios e linha de comando.
          Usuários(as) novatos(as) podem querer ler primeiro a
          <a href="user-manuals#quick-reference">referência
          Debian</a>.</dd>
        </dl>
    </dd>

</dl>


<hrline />

<p>Se você conferiu os recursos acima e ainda não encontrou as respostas para
suas perguntas ou soluções para os seus problemas no Debian, dê uma
olhada na nossa <a href="../support">página de suporte</a>.</p>
