#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Ming.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7866">CVE-2018-7866</a>

<p>Déréférencement de pointeur NULL dans la fonction newVar3 (util/decompile.c).
Des attaquants distants peuvent exploiter cette vulnérabilité pour provoquer
un déni de service à l'aide d'un fichier swf contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7873">CVE-2018-7873</a>

<p>Vulnérabilité de dépassement de tampon basé sur le tas dans la fonction
getString (util/decompile.c). Des attaquants distants peuvent exploiter cette
vulnérabilité pour provoquer un déni de service à l'aide d'un fichier swf
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7876">CVE-2018-7876</a>

<p>Dépassement d’entier et épuisement de mémoire subséquente dans la fonction
parseSWF_ACTIONRECORD (util/parser.c). Des attaquants distants peuvent exploiter
cette vulnérabilité pour provoquer un déni de service à l'aide d'un fichier swf
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9009">CVE-2018-9009</a>

<p>Divers vulnérabilités de dépassement de tampon basé sur le tas dans
util/decompiler.c. Des attaquants distants peuvent exploiter cette vulnérabilité
pour provoquer un déni de service à l'aide d'un fichier swf contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-9132">CVE-2018-9132</a>

<p>Déréférencement de pointeur NULL dans la fonction getInt (util/decompile.c).
Des attaquants distants peuvent exploiter cette vulnérabilité pour provoquer un
déni de service à l'aide d'un fichier swf contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1:0.4.4-1.1+deb7u9.</p>
<p>Nous vous recommandons de mettre à jour vos paquets ming.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1386.data"
# $Id: $
