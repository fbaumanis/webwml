#use wml::debian::translation-check translation="c5076e723aa992958ffc10724f43606532a06dc3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>libgc, un ramasse-miettes conservateur, est vulnérable à un dépassement
d'entiers à de multiples emplacements. Dans certains cas, lorsque
l'allocation d'une quantité énorme de mémoire est demandée, plutôt que de
faire échouer la requête, libgc renvoie un pointeur vers une petite
quantité de mémoire entraînant éventuellement l'application dans un
écrasement de tampon.</p>


<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1:7.4.2-8+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libgc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libgc, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libgc">\
https://security-tracker.debian.org/tracker/libgc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2966.data"
# $Id: $
