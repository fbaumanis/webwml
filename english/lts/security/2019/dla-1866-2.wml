<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Simon McVittie spotted a memory leak regression in the way <a href="https://security-tracker.debian.org/tracker/CVE-2019-13012">CVE-2019-13012</a> had been resolved
for glib2.0 in Debian jessie.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.42.1-1+deb8u3.</p>

<p>We recommend that you upgrade your glib2.0 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1866-2.data"
# $Id: $
