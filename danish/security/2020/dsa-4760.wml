#use wml::debian::translation-check translation="669c87408de3af72c047aaa7ef3786903984b7ba" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Adskillige sikkerhedsproblemer blev opdaget i QEMU, en hurtig 
processoremulator:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12829">CVE-2020-12829</a>

    <p>Et heltalsoverløb i sm501-displayenheden kunne medføre 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14364">CVE-2020-14364</a>

    <p>En skrivning udenfor grænserne i USB-emuleringskoden kunne medføre 
    gæst til vært-kodeudførelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15863">CVE-2020-15863</a>

    <p>Et bufferoverløb i XGMAC-netværksdriveren kunne medføre lammelsesangreb 
    eller udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16092">CVE-2020-16092</a>

    <p>En udløsbar assert i e1000e- og vmxnet3-enhederne kunne medføre 
    lammelsesangreb.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 1:3.1+dfsg-8+deb10u8.</p>

<p>Vi anbefaler at du opgraderer dine qemu-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende qemu, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/qemu">\
https://security-tracker.debian.org/tracker/qemu</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4760.data"
