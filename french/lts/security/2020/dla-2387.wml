#use wml::debian::translation-check translation="f6086d5cfaebf1b0a16a4e5337d5718a05937dc1" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans le navigateur web
Firefox de Mozilla, qui pourraient éventuellement aboutir à l'exécution de code
arbitraire, à un script intersite ou à une usurpation de l’origine du
téléchargement.</p>

<p>Debian suit la prise en charge étendue (ESR) des publications de Firefox. La
prise en charge pour les séries 68.x est terminée, aussi avec cette mise à jour,
nous suivons les publications 78.x.</p>

<p>Entre les version 68.x et 78.x, Firefox a vu un certain nombre de mises
à jour de ses fonctions. Pour plus d’informations, veuillez consulter
<a href="https://www.mozilla.org/en-US/firefox/78.0esr/releasenotes/">https://www.mozilla.org/en-US/firefox/78.0esr/releasenotes/</a>.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 78.3.0esr-1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firefox-esr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firefox-esr, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firefox-esr">https://security-tracker.debian.org/tracker/firefox-esr</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2387.data"
# $Id: $
