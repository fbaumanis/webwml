<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Linux 4.9 has been packaged for Debian 8 as linux-4.9.  This provides
a supported upgrade path for systems that currently use kernel
packages from the "jessie-backports" suite.</p>

<p>There is no need to upgrade systems using Linux 3.16, as that kernel
version will also continue to be supported in the LTS period.</p>

<p>This backport does not include the following binary packages:</p>

    <p>hyperv-daemons libcpupower1 libcpupower-dev libusbip-dev
    linux-compiler-gcc-4.9-x86 linux-cpupower linux-libc-dev usbip</p>

<p>Older versions of most of those are built from other source packages
in Debian 8.</p>

<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5753">CVE-2017-5753</a>

    <p>Further instances of code that was vulnerable to Spectre variant 1
    (bounds-check bypass) have been mitigated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18255">CVE-2017-18255</a>

    <p>It was discovered that the performance events subsystem did not
    properly validate the value of the
    kernel.perf_cpu_time_max_percent sysctl.  Setting a large value
    could have an unspecified security impact.  However, only a
    privileged user can set this sysctl.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1118">CVE-2018-1118</a>

    <p>The syzbot software found that the vhost driver did not initialise
    message buffers which would later be read by user processes.  A
    user with access to the /dev/vhost-net device could use this to
    read sensitive information from the kernel or other users'
    processes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1120">CVE-2018-1120</a>

    <p>Qualys reported that a user able to mount FUSE filesystems can
    create a process such that when another process attempting to read
    its command line will be blocked for an arbitrarily long time.
    This could be used for denial of service, or to aid in exploiting
    a race condition in the other program.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1130">CVE-2018-1130</a>

    <p>The syzbot software found that the DCCP implementation of
    sendmsg() does not check the socket state, potentially leading
    to a null pointer dereference.  A local user could use this to
    cause a denial of service (crash).    </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">CVE-2018-3639</a>

    <p>Multiple researchers have discovered that Speculative Store Bypass
    (SSB), a feature implemented in many processors, could be used to
    read sensitive information from another context.  In particular,
    code in a software sandbox may be able to read sensitive
    information from outside the sandbox.  This issue is also known as
    Spectre variant 4.</p>

    <p>This update allows the issue to be mitigated on some x86
    processors by disabling SSB.  This requires an update to the
    processor's microcode, which is non-free.  It may be included in
    an update to the system BIOS or UEFI firmware, or in a future
    update to the intel-microcode or amd64-microcode packages.</p>

    <p>Disabling SSB can reduce performance significantly, so by default
    it is only done in tasks that use the seccomp feature.
    Applications that require this mitigation should request it
    explicitly through the prctl() system call.  Users can control
    where the mitigation is enabled with the spec_store_bypass_disable
    kernel parameter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5814">CVE-2018-5814</a>

    <p>Jakub Jirasek reported race conditions in the USB/IP host driver.
    A malicious client could use this to cause a denial of service
    (crash or memory corruption), and possibly to execute code, on a
    USB/IP server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10021">CVE-2018-10021</a>

    <p>A physically present attacker who unplugs a SAS cable can cause a
    denial of service (memory leak and WARN).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10087">CVE-2018-10087</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2018-10124">CVE-2018-10124</a>

    <p>zhongjiang found that the wait4() and kill() system call
    implementations did not check for the invalid pid value of
    INT_MIN.  If a user passed this value, the behaviour of the code
    was formally undefined and might have had a security impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10853">CVE-2018-10853</a>

    <p>Andy Lutomirski and Mika Penttilä reported that KVM for x86
    processors did not perform a necessary privilege check when
    emulating certain instructions.  This could be used by an
    unprivileged user in a guest VM to escalate their privileges
    within the guest.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10876">CVE-2018-10876</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-10877">CVE-2018-10877</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-10878">CVE-2018-10878</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-10879">CVE-2018-10879</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10880">CVE-2018-10880</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-10881">CVE-2018-10881</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-10882">CVE-2018-10882</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-10883">CVE-2018-10883</a></p>

    <p>Wen Xu at SSLab, Gatech, reported that crafted ext4 filesystem
    images could trigger a crash or memory corruption.  A local user
    able to mount arbitrary filesystems, or an attacker providing
    filesystems to be mounted, could use this for denial of service or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10940">CVE-2018-10940</a>

    <p>Dan Carpenter reported that the optical disc driver (cdrom) does
    not correctly validate the parameter to the CDROM_MEDIA_CHANGED
    ioctl.  A user with access to a cdrom device could use this to
    cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11506">CVE-2018-11506</a>

    <p>Piotr Gabriel Kosinski and Daniel Shapira reported that the
    SCSI optical disc driver (sr) did not allocate a sufficiently
    large buffer for sense data.  A user with access to a SCSI
    optical disc device that can produce more than 64 bytes of
    sense data could use this to cause a denial of service (crash
    or memory corruption), and possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12233">CVE-2018-12233</a>

    <p>Shankara Pailoor reported that a crafted JFS filesystem image
    could trigger a denial of service (memory corruption).  This
    could possibly also be used for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000204">CVE-2018-1000204</a>

    <p>The syzbot software found that the SCSI generic driver (sg) would
    in some circumstances allow reading data from uninitialised
    buffers, which could include sensitive information from the kernel
    or other tasks.  However, only privileged users with the
    CAP_SYS_ADMIN or CAP_SYS_RAWIO capability were allowed to do this,
    so this has little or no security impact.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.9.110-1~deb8u1.  This update additionally fixes Debian bugs
#860900, #872907, #892057, #896775, #897590, and #898137; and
includes many more bug fixes from stable updates 4.9.89-4.9.110
inclusive.</p>

<p>We recommend that you upgrade your linux-4.9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1423.data"
# $Id: $
