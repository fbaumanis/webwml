# Emmanuel Galatoulas <galas@tee.gr>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2019-07-14 01:34+0200\n"
"Last-Translator: Emmanuel Galatoulas <galas@tee.gr>\n"
"Language-Team: Greek <debian-www@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/ports/alpha/menu.inc:6
msgid "Debian for Alpha"
msgstr "Debian για Alpha"

#: ../../english/ports/hppa/menu.inc:6
msgid "Debian for PA-RISC"
msgstr "Debian για PA-RISC"

#: ../../english/ports/hurd/menu.inc:10
msgid "Hurd CDs"
msgstr "CD για Hurd"

#: ../../english/ports/ia64/menu.inc:6
msgid "Debian for IA-64"
msgstr "Debian για IA-64"

#: ../../english/ports/menu.defs:11
msgid "Contact"
msgstr "Επικοινωνία"

#: ../../english/ports/menu.defs:15
msgid "CPUs"
msgstr "Επεξεργαστές"

#: ../../english/ports/menu.defs:19
msgid "Credits"
msgstr "Εύσημα"

#: ../../english/ports/menu.defs:23
msgid "Development"
msgstr "Ανάπτυξη"

#: ../../english/ports/menu.defs:27
msgid "Documentation"
msgstr "Τεκμηρίωση"

#: ../../english/ports/menu.defs:31
msgid "Installation"
msgstr "Εγκατάσταση"

#: ../../english/ports/menu.defs:35
msgid "Configuration"
msgstr "Ρύθμιση"

#: ../../english/ports/menu.defs:39
msgid "Links"
msgstr "Σύνδεσμοι"

#: ../../english/ports/menu.defs:43
msgid "News"
msgstr "Νέα"

#: ../../english/ports/menu.defs:47
msgid "Porting"
msgstr "Εκδόσεις"

#: ../../english/ports/menu.defs:51
msgid "Ports"
msgstr "Εκδοχές"

#: ../../english/ports/menu.defs:55
msgid "Problems"
msgstr "Προβλήματα"

#: ../../english/ports/menu.defs:59
msgid "Software Map"
msgstr "Χάρτης Λογισμικού"

#: ../../english/ports/menu.defs:63
msgid "Status"
msgstr "Κατάσταση"

#: ../../english/ports/menu.defs:67
msgid "Supply"
msgstr "Παροχή"

#: ../../english/ports/menu.defs:71
msgid "Systems"
msgstr "Συστήματα"

#: ../../english/ports/netbsd/menu.inc:6
msgid "Debian GNU/NetBSD for i386"
msgstr "Debian GNU/NetBSD για i386"

#: ../../english/ports/netbsd/menu.inc:10
msgid "Debian GNU/NetBSD for Alpha"
msgstr "Debian GNU/NetBSD για Alpha"

#: ../../english/ports/netbsd/menu.inc:14
msgid "Why"
msgstr "Γιατί"

#: ../../english/ports/netbsd/menu.inc:18
msgid "People"
msgstr "Ανθρωποι"

#: ../../english/ports/powerpc/menu.inc:6
msgid "Debian for PowerPC"
msgstr "Debian για PowerPC"

#: ../../english/ports/sparc/menu.inc:6
msgid "Debian for Sparc"
msgstr "Debian για Sparc"

#~ msgid "Clock"
#~ msgstr "Ρολόι"

#~ msgid "DCache"
#~ msgstr "DCache"

#~ msgid "Date announced"
#~ msgstr "Ημερομηνία ανακοίνωσης"

#~ msgid "Debian GNU/FreeBSD"
#~ msgstr "Debian GNU/FreeBSD"

#~ msgid "Debian for AMD64"
#~ msgstr "Debian για AMD64"

#~ msgid "Debian for ARM"
#~ msgstr "Debian για ARM"

#~ msgid "Debian for Beowulf"
#~ msgstr "Debian για Beowulf"

#~ msgid "Debian for Laptops"
#~ msgstr "Debian για Φορητούς"

#~ msgid "Debian for MIPS"
#~ msgstr "Debian για MIPS"

#~ msgid "Debian for Motorola 680x0"
#~ msgstr "Debian για Motorola 680x0"

#~ msgid "Debian for S/390"
#~ msgstr "Debian για S/390"

#~ msgid "Debian for Sparc64"
#~ msgstr "Debian για Sparc64"

#~ msgid "ICache"
#~ msgstr "ICache"

#~ msgid "ISA"
#~ msgstr "ISA"

#~ msgid "Main"
#~ msgstr "Κύρια"

#~ msgid "No FPU (R2010), external caches"
#~ msgstr "Χωρίς FPU (R2010), εξωτερικές caches"

#~ msgid "No FPU (R3010)"
#~ msgstr "Χωρίς FPU (R3010)"

#~ msgid "Specials"
#~ msgstr "Εξειδικευμένα"

#~ msgid "TLB"
#~ msgstr "TLB"

#~ msgid "Vendor/Name"
#~ msgstr "Πωλητής/Ονομα"

#~ msgid "libc5-based Debian GNU/FreeBSD"
#~ msgstr "βασισμένο σε libc5 Debian GNU/FreeBSD"
