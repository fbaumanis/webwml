#use wml::debian::template title="Debian Website in different Languages" MAINPAGE="true"
#use wml::debian::toc

<define-tag toc-title-formatting endtag="required">%body</define-tag>
<define-tag toc-item-formatting endtag="required">[%body]</define-tag>
<define-tag toc-display-begin><p></define-tag>
<define-tag toc-display-end></p></define-tag>

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#intro">Content Navigation</a></li>
    <li><a href="#howtoset">How to set a Web Browser's Language</a></li>
    <li><a href="#override">How to override the Settings</a></li>
    <li><a href="#fix">Troubleshooting</a></li>
  </ul>
</div>

<h2><a id="intro">Content Navigation</a></h2>

<p>
A team of <a href="../devel/website/translating">translators</a>
works on the Debian website to convert it for a growing
number of different languages. But how does the language
switch in the web browser work? A standard called 
<a href="$(HOME)/devel/website/content_negotiation">content negotiation</a>
allows users to set their preferred language(s) for web content. The
version they see is negotiated between the web browser and the web server:
the browser sends the preferences to the server, and the server then
decides which version to deliver (based on the users' preferences and
the available versions).
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="http://www.w3.org/International/questions/qa-lang-priorities">Read more at W3C</a></button></p>

<p>
Not everyone knows about content negotiation, so links at the bottom
of every Debian page point to other available versions. Please note
that selecting a different language from this list will only affect
the current page. It doesn't change the default language of your web
browser. If you follow another link to a different page, you will see
it in the default language again.
</p>

<p>
In order to change your default language, you have two options:
</p>

<ul>
  <li><a href="#howtoset">Configure your web browser</a></li>
  <li><a href="#override">Override your browser's language preferences</a></li>
</ul>

<p>
Jump straight to the configuration instructions for these web browsers:</p>

<toc-display />

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> The original language of the Debian website is English. Therefore, it's a good idea to add English (<code>en</code>) at the bottom of your language list. This works as a backup in case a page hasn't yet been translated into any of your preferred languages.</p>
</aside>

<h2><a id="howtoset">How to set a Web Browser's Language</a></h2>

<p>
Before we describe how to configure the language settings in different
web browsers, some general remarks. Firstly, it's a good idea to include
all languages you speak to your list of preferred languages. For example,
if you're a native French speaker, you can choose <code>fr</code> as your
first language, followed by English with the language code <code>en</code>.
</p>

<p>
Secondly, in some browsers you can enter language codes instead of
choosing from a menu. If that's the case, keep in mind that creating
a list like <code>fr, en</code> doesn't define your preference. Instead,
it will define equally ranked options, and the web server can decide
to ignore the order and just pick one of the languages. If you want to
specify a real preference, you have to work with so-called quality values,
i.e. floating point values between 0 and 1. A higher value indicates a
higher priority. If we go back to the example with French and English
language, you can modify the above example like this:
</p>

<pre>
fr; q=1.0, en; q=0.5
</pre>

<h3>Be careful with Country Codes</h3>

<p>
A web server which receives a request for a document with the preferred
language <code>en-GB, fr</code> does <strong>not always</strong> serve the
English version before the French one. It will only do so if a page
with the language extension <code>en-gb</code> exists. It does work the
other way around, though: a server can return a <code>en-us</code> page if
just <code>en</code> is included in the preferred languages list.
</p>

<p>
We therefore recommend not to add two-letter country codes like
<code>en-GB</code> or <code>en-US</code>, unless you have a very good reason. If
you do add one, make sure to include the language code without the
extension as well: <code>en-GB, en, fr</code>
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://httpd.apache.org/docs/current/content-negotiation.html">More about Content Negotiation</a></button></p>

<h3>Instructions for different Web Browsers</h3>

<p>
We've compiled a list of popular web browsers and some instructions on how to 
change the preferred language for web content in their settings:
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="chromium">Chrome/Chromium</toc-add-entry></strong><br>
  At the top right, open the menu and click <em>Settings</em> -&gt; <em>Advanced</em> -&gt; <em>Languages</em>. Open the <em>Language</em> menu to see a list of languages. Click on the three dots next to an entry to change the order. You can also add new languages if necessary.</li>
        <li><strong><toc-add-entry name="elinks">ELinks</toc-add-entry></strong><br>
  Setting the default language via <em>Setup</em> -&gt; <em>Language</em> will also change the requested language from web sites. You can change this behavior and fine-tune the <em>Accept-Language header</em> at <em>Setup</em> -&gt; <em>Options manager</em> -&gt; <em>Protocols</em> -&gt; <em>HTTP</em></li>
        <li><strong><toc-add-entry name="epiphany">Epiphany</toc-add-entry></strong><br>
  Open <em>Preferences</em> from the main menu and switch to the <em>Language</em> tab. Here you can add, remove and arrange languages.</li>
        <li><strong><toc-add-entry name="mozillafirefox">Firefox</toc-add-entry></strong><br>
  In the menu bar at the top open <em>Preferences</em>. Scroll down to <em>Language and appearance</em> -&gt; <em>Language</em> in the <em>General</em> panel. Click the button <em>Choose</em> to set your preferred language for displaying websites. In the same dialog you can also add, remove and reorder languages.</li>
        <li><strong><toc-add-entry name="ibrowse">IBrowse</toc-add-entry></strong><br>
  Go into <em>Preferences</em> -&gt; <em>Settings</em> -&gt; <em>Network</em>. <em>Accept language</em> probably shows a * which is the default. If you click on the <em>Locale</em> button, you should be able to add your preferred language. If not, you can enter it manually.</li>
        <li><strong><toc-add-entry name="icab">iCab</toc-add-entry></strong><br>
  <em>Edit</em> -&gt; <em>Preferences</em> -&gt; <em>Browser</em> -&gt; <em>Fonts, Languages</em></li>
        <li><strong><toc-add-entry name="iceweasel">IceCat (Iceweasel)</toc-add-entry></strong><br>
  <em>Edit</em> -&gt; <em>Preferences</em> -&gt; <em>Content</em> -&gt; <em>Languages</em> -&gt; <em>Choose</em></li>
        <li><strong><toc-add-entry name="ie">Internet Explorer</toc-add-entry></strong><br>
  Click the <em>Tools</em> icon, select <em>Internet options</em>, switch to the <em>General</em> tab, and click the <em>Languages</em> button. Click <em>Set Language Preferences</em>, and in the following dialog you can add, remove and re-order languages.</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><strong><toc-add-entry name="konqueror">Konqueror</toc-add-entry></strong><br>
        Edit the file <em>~/.kde/share/config/kio_httprc</em> and include the following new line:<br>
        <code>Languages=fr;q=1.0, en;q=0.5</code></li>
        <li><strong><toc-add-entry name="lynx">Lynx</toc-add-entry></strong><br>
        Edit the file <em>~/.lynxrc</em> and enter the following line:<br>
        <code>preferred_language=fr; q=1.0, en; q=0.5</code><br>
        Alternatively, you can open the browser's settings by pressing [O]. Scroll down to <em>Preferred language</em> and add the above.</li>
        <li><strong><toc-add-entry name="edge">Microsoft Edge</toc-add-entry></strong><br>
        <em>Settings and more</em>  -&gt; <em>Settings</em> -&gt; <em>Languages</em> -&gt; <em>Add languages</em><br>
        Click the three-dotted button next to a language entry for more options and to change the order.</li>
        <li><strong><toc-add-entry name="opera">Opera</toc-add-entry></strong><br>
        <em>Settings</em> -&gt; <em>Browser</em> -&gt; <em>Languages</em> -&gt; <em>Preferred languages</em></li>
        <li><strong><toc-add-entry name="safari">Safari</toc-add-entry></strong><br>
        Safari uses the system-wide settings on macOS and iOS, so in order to define your preferred language, please open <em>System Preferences</em> (macOS) or <em>Settings</em> (iOS).</li>
        <li><strong><toc-add-entry name="w3m">W3M</toc-add-entry></strong><br>
        Press [O] to open the <em>Option Setting Panel</em>, scroll down to <em>Network Settings</em> -&gt; <em>Accept-Language header</em>. Press [Enter] to change the settings (for example <code>fr; q=1.0, en; q=0.5</code>) and confirm with [Enter]. Scroll all the way down to [OK] to save your settings.</li>
        <li><strong><toc-add-entry name="vivaldi">Vivaldi</toc-add-entry></strong><br>
        Go to <em>Settings</em> -&gt; <em>General</em> -&gt; <em>Language</em> -&gt; <em>Accepted Languages</em>, click <em>Add Language</em> and choose one from the menu. Use the arrows to change the order of your preferences.</li>
      </ul>
    </div>
  </div>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> While it's always better to select your preferred language in the browser configuration, there is an option to override the settings with a cookie.</p>
</aside>

<h2><a id="override">How to override the Settings</a></h2>

<p>
If for whatever reason you're not able to define your preferred language
in the browser's settings, device or computing environment, you can
override the preferences using a cookie as a last resort. Click one of
the buttons below to put a single language on top of the list.
</p>

<p>
Please note that this will set a <a
href="https://en.wikipedia.org/wiki/HTTP_cookie">cookie</a>. Your
browser will automatically delete it if you don't visit this website
for a month. Of course, you can always delete the cookie manually in
your web browser or by clicking the button <em>Browser default</em>.
</p>

<protect pass=2>
<: print language_selector_buttons(); :>
</protect>


<h2><a id="fix">Troubleshooting</a></h2>

<p>
Sometimes the Debian website appears in the wrong language despite
all efforts to set a preferred language. Our first suggestion is
to clean out the local cache (both disk and memory) in your browser
before trying to reload the website.  If you're absolutely sure that
you've <a href="#howtoset">configured your browser</a> properly, then a
broken or a misconfigured cache might be the problem. This is becoming
a serious issue these days, as more and more ISPs see caching as a way
of decreasing their net traffic. Read the <a href="#cache">section</a>
about proxy servers even if you don't think you're using one.
</p>

<p>
By all means, it's always possible that there is a problem with <a
href="https://www.debian.org/">www.debian.org</a>. While only a
handful of language problems reported in the last years were caused
by a bug on our end, it is entirely possible. We therefore suggest
that you investigate your own settings and a potential caching issue
first, before you <a href="../contact">get in touch</a> with us. If <a
href="https://www.debian.org/">https://www.debian.org/</a> is working,
but one of the <a href="https://www.debian.org/mirror/list">mirrors</a>
is not, please report this so we can contact the mirror maintainers.
</p>

<h3><a name="cache">Potential Problems with Proxy Servers</a></h3>

<p>
Proxy servers are essentially web servers that have no content of
their own. They sit in the middle between users and real web servers,
grab the requests for web pages, and fetch the pages. After that, they
forward the content to the users' web browsers, but also make a local,
cached copy which is used for later requests. This can really cut down
on network traffic when many users request the same page.
</p>

<p>
While this can be a good idea most of the time, it also causes failures
when the cache is buggy. In particular, some older proxy servers do not
understand content negotiation. This results in them caching a page in
one language and serving that, even if a different language is requested
later. The only solution is to upgrade or replace the caching software.
</p>

<p>
Historically, proxy servers were only used when people configured their
web browser accordingly. However, this is no longer the case. Your ISP
may be redirecting all HTTP requests through a transparent proxy. If
the proxy doesn't handle content negotiation properly, then users can
receive cached pages in the wrong language. The only way you can fix
this is to complain to your ISP in order for them to upgrade or replace
their software.
</p>
