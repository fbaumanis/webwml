<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in libmodbus, a library for the Modbus
protocol.
Both issues are related to out of bound reads, which could result in a
denial of service or other unspecified impact.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
3.0.6-2+deb9u1.</p>

<p>We recommend that you upgrade your libmodbus packages.</p>

<p>For the detailed security status of libmodbus please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libmodbus">https://security-tracker.debian.org/tracker/libmodbus</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2825.data"
# $Id: $
