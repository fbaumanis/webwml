#use wml::debian::translation-check translation="1d406bd4e2a25e896592eb7d516b0f06d7ef8946" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2020-27783">CVE-2020-27783</a>,
publié dans la DLA 2467-1, était incomplet car le composant &lt;math/svg&gt;
était toujours affecté par la vulnérabilité. Cette mise à jour fournit un
additif qui complète la correctif. Il est à remarquer que le paquet de
version 3.7.1-1+deb9u2 a été téléversé, mais avant la publication de l’annonce
une régression a été découverte, qui a été immédiatement corrigée avant la
publication de cette annonce.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 3.7.1-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lxml.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de lxml, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/lxml">https://security-tracker.debian.org/tracker/lxml</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2467-2.data"
# $Id: $
