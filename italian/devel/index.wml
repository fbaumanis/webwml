#use wml::debian::template title="L'angolo degli sviluppatori" MAINPAGE="true"
#use wml::debian::translation-check translation="fca004f92b97a053bc89c121827943f1eae0531b" maintainer="Giuseppe Sacco"

# $Id$

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>
Nonostante le informazioni di questa pagina e tutti i collegamenti ad altre
pagine siano pubblici, questo sito è destinato principalmente agli
sviluppatori Debian.
</p>
</aside>

<ul class="toc">
<li><a href="#basic">Notizie di base</a></li>
<li><a href="#packaging">Sui pacchetti</a></li>
<li><a href="#workinprogress">Lavori in corso</a></li>
<li><a href="#projects">Progetti</a></li>
<li><a href="#miscellaneous">Varie</a></li>
</ul>

<div class="row">
  <!-- left column -->
  <div class="column column-left" id="basic">
    <div style="text-align: center">
      <span class="fa fa-users fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="basic">Informazioni generali</a></h2>
      <p>Un elenco degli attuali sviluppatori e manutentori, come unirsi al progetto e collegamenti
      al database degli sviluppatori, la costituzione, la modalità di voto, i rilasci e le
      architetture.</p>
    </div>

    <div style="text-align: left">
      <dl>
        <dt><a href="$(HOME)/intro/organization">Organizzazione Debian</a></dt>
        <dd>Oltre un migliaio di volontari compongono il progetto Debian. Questa pagina mostra la
        struttura dell'organizzazione Debian, elenca i gruppi e i loro membri oltre al
        loro indirizzo per contatti.</dd>
        <dt><a href="$(HOME)/intro/people">Le persone dietro Debian</a></dt>
        <dd>Gli <a href="https://wiki.debian.org/DebianDeveloper">Sviluppatori Debian («Debian Developer»
        o DD)</a>, cioè i membri a pieno titolo del progetto Debian, e i <a
        href="https://wiki.debian.org/DebianMaintainer">manutentori Debian («Debian Maintainer» o DM)</a>
        contribuiscono al progetto. Per sapere di più sulle persone coinvolte, inclusi i pacchetti
        che gestiscono, guardare <a href="https://nm.debian.org/public/people/dd_all/">l'elenco degli
        sviluppatori Debian</a> e <a href="https://nm.debian.org/public/people/dm_all/">l'elenco dei
        manutentori</a>. Abbiamo anche una <a href="developers.loc">mappa mondiale degli sviluppatori
        Debian</a> e una <a href="https://gallery.debconf.org/">galleria</a> con immagini di alcuni
        eventi Debian.</dd>
        <dt><a href="join/">Come unirsi a Debian</a></dt>
        <dd>Si vuole contribuire e unirsi al progetto? Siamo sempre in cerca di nuovi sviluppatori
        o persone entusiaste del software libero e con capacità tecniche. Per maggiori informazioni
        visitare questa pagina.</dd>
        <dt><a href="https://db.debian.org/">Database degli sviluppatori</a></dt>
        <dd>Alcune parti di questo database sono accessibili a tutti, altre lo sono solo per gli
        svilppatori che abbiano fatto l'accesso al sistema. Il database contiene informazioni quali
        <a href="https://db.debian.org/machines.cgi">le macchine del progetto</a> e <a href="extract_key">le
        chiavi GnuPG degli sviluppatori</a>. Gli sviluppatori con un account possono <a
        href="https://db.debian.org/password.html">cambiare la propria password</a> e imparare come
        impostare <a href="https://db.debian.org/forward.html">l'inoltro dell'email</a> per il loro
        account Debian. Se si ipotizza di usare una delle macchine Debian, accertarsi di aver letto
        le <a href="dmup">norme per l'utilizzo delle macchine Debian</a>.</dd>
        <dt><a href="constitution">La constituzione</a></dt>
        <dd>Questo documento descrive la struttura organizzativa su come il progetto prende
        le decisioni.
        </dd>
        <dt><a href="$(HOME)/vote/">Informazioni sulle votazioni</a></dt>
        <dd>Come eleggiamo il nostro «leader», scegliamo i nostri loghi e come si vota in genere.</dd>
        <dt><a href="$(HOME)/releases/">Rilasci</a></dt>
        <dd>Questa pagina elenca i nostri rilasci (<a href="$(HOME)/releases/stable/">stable</a>,
        <a href="$(HOME)/releases/testing/">testing</a> e <a href="$(HOME)/releases/unstable/">unstable</a>)
        e contiene un indice di quelli precedenti e dei loro nomi in codice.</dd>
        <dt><a href="$(HOME)/ports/">Architetture diverse</a></dt>
        <dd>Debian gira su molte architetture. Questa pagina raccoglie informazioni sui vari
        «port» Debian, alcuni basati sul kernel Linux, altri su quelli FreeBSD, NetBSD e Hurd.</dd>
     </dl>
    </div>
 
   </div>
  <!-- right column -->
  <div class="column column-right" id="software">
    <div style="text-align: center">
      <span class="fa fa-code fa-3x" style="float:left;margin-right:5px;margin-bottom:5px;"></span>
      <h2><a id="packaging">Pacchettizzare</a></h2>
      <p>Collegamenti al nostro manuale sulla «policy» e altri documenti su «policy», procedure e altre
      risorse per sviluppatori Debian, e la guida per il nuovo manutentore.</p>
    </div>
 
    <div style="text-align: left">
      <dl>
        <dt><a href="$(DOC)/debian-policy/">Manuale sulla «policy» Debian</a></dt>
        <dd>Questo manuale descrive i requisiti per la distribuzione Debian. Include la struttura
        e il contenuto dell'archivio Debian, alcuni punti d'attenzione sul sistema operativo oltre
        che requisiti tecnici che ogni pacchetto deve soddisfare per essere incluso nella
        distribuzione.
        <p>In poche parole, <strong>bisogna</strong> leggerlo.</p>
        </dd>
      </dl>
      <p>Ci sono alcuni altri documenti collegati alla «policy» ai quali si potrebbe essere
      interessati:</p>
      <ul>
         <li><a href="https://wiki.linuxfoundation.org/lsb/fhs/">Filesystem Hierarchy Standard</a> (FHS)
         <br /> lo FHS definisce la struttura delle directory e il loro contenuto (la posizione dei file);
         l'aderenza alla versione 3.0 è obbligatoria (vedere il <a
         href="https://www.debian.org/doc/debian-policy/ch-opersys.html#file-system-hierarchy">capitolo
         9</a> della manuale sulla «policy» Debian).</li>
         <li>Elenco di <a href="$(DOC)/packaging-manuals/build-essential">pacchetti «build-essential»</a>
         <br />Se si vuole compilare software, si può fare affidamento che questi pacchetti siano
         già installati. Non vanno inclusi nella linea <code>Build-Depends</code> quando si <a
         href="https://www.debian.org/doc/debian-policy/ch-relationships.html">dichiarano le relazioni</a>
         tra pacchetti.</li>
         <li><a href="$(DOC)/packaging-manuals/menu-policy/">Il sistema dei menu</a>
        <br />La struttura dei menu Debian; controllare anche la documentazione sul
        <a href="$(DOC)/packaging-manuals/menu.html/">sistema dei menu</a>.</li>
        <li><a href="$(DOC)/packaging-manuals/debian-emacs-policy">Emacs Policy</a></li>
        <li><a href="$(DOC)/packaging-manuals/java-policy/">Java Policy</a></li>
        <li><a href="$(DOC)/packaging-manuals/perl-policy/">Perl Policy</a></li>
        <li><a href="$(DOC)/packaging-manuals/python-policy/">Python Policy</a></li>
        <li><a href="$(DOC)/packaging-manuals/debconf_specification.html">La specifica Debconf</a></li>
        <li><a href="https://www.debian.org/doc/manuals/dbapp-policy/">Database Application Policy</a> (bozza)</li>
        <li><a href="https://tcltk-team.pages.debian.net/policy-html/tcltk-policy.html/">Tcl/Tk Policy</a> (bozza)</li>
        <li><a href="https://people.debian.org/~lbrenta/debian-ada-policy.html">Debian Policy per Ada</a></li>
       </ul>
       <p>Leggere anche le <a href="https://bugs.debian.org/debian-policy">proposte di aggiornamento della
       «policy» Debian</a>.</p>
       <dl>
       <dt><a href="$(DOC)/manuals/developers-reference/">Riferimenti per gli sviluppatori</a></dt>
       <dd>
       Procedure raccomandate e risorse disponibili per gli sviluppatori Debian
       -- un'altra <strong>lettura obbligatoria</strong>.
       </dd>
       <dt><a href="$(DOC)/manuals/debmake-doc/">Guida per il nuovo manutentore</a></dt>
       <dd>
       Come costruire un pacchetto Debian (in parole semplici), con molti esempi. Se si pensa
       di diventare uno sviluppare Debian o un manutentore, questo è un buon punto di partenza.
       </dd>
     </dl>
    </div>
  </div>
</div>

<h2><a id="workinprogress">Lavori in corso: collegamenti per sviluppatori e manutentori Debian attivi</a></h2>

<aside class="light">
  <span class="fas fa-wrench fa-5x"></span>
</aside>

<dl>
  <dt><a href="testing">Debian &lsquo;Testing&rsquo;</a></dt>
  <dd>
    Viene generata automaticamente dalla distribuzione &lsquo;unstable&rsquo;:
    questo è il posto dove far arrivare i propri pacchetti per far sì che
    siano presi in considerazione per il prossimo rilascio Debian.
  </dd>

  <dt><a href="https://bugs.debian.org/release-critical/">Release Critical Bugs</a></dt>
  <dd>
    Un elenco di bug che causano la rimozione del pacchetto della distribuzione
    &lsquo;testing&rsquo; o possono causare ritardi nel rilascio successivo. Le segnalazioni
    di bug con una severità più altao uguale a &lsquo;serious&rsquo; possono finire in
    quest'elenco, quindi si deve fare attenzione a risolvere questi bug nei propri
    pacchetti il più in fretta possibile.
  </dd>
  <dt><a href="$(HOME)/Bugs/">Debian Bug Tracking System (BTS)</a></dt>
    <dd>
    Per segnalare, discutere e risolvere bug. Il BTS è utile sia agli utenti che agli sviluppatori.
    </dd>
  <dt>Informazioni sui pacchetti Debian</dt>
    <dd>
      Le pagine web <a href="https://qa.debian.org/developer.php">informazioni sui pacchetti</a>
      e <a href="https://tracker.debian.org/">tracciamento dei pacchetti</a> raccolgono informazioni
      importanti per il manutentore. Lo sviluppatore che voglia tenere traccia di altri pacchetti
      può iscriversi (via email) al servizio che le manda copie dei messaggi del BTS e le notifiche
      di caricamenti e installazioni. Leggere il <a
      href="$(DOC)/manuals/developers-reference/resources.html#pkg-tracker">manuale del tracciamento
      dei pacchetti</a> per altre informazioni.
    </dd>
  <dt><a href="wnpp/">Pacchetti che necessitano aiuto</a></dt>
    <dd>
      «Work-Needing and Prospective Packages», detti anche WNPP. Si tratta di un elenco di
      pacchetti Debian che necessitano di un nuovo manutentore e pacchetti che non sono
      ancora stati inclusi in Debian.
    </dd>

  <dt><a href="$(DOC)/manuals/developers-reference/resources.html#incoming-system">Sistema
  «incoming»</a></dt>
     <dd>
     Server interni dell'archivio: questo è il posto dove i nuovi pacchetti vengono caricati.
     I pacchetti accettati sono quasi subito resi disponibili via browser e propagati ai
     <a href="$(HOME)/mirror/">mirror</a> quattro volte al giorno.
     <br />
     <strong>Nota:</strong> a caua della natura di &lsquo;incoming&rsquo;, si raccomanda
     di non copiarla sui mirror.
     </dd>

   <dt><a href="https://lintian.debian.org/">Resoconti lintian</a></dt>
      <dd>
      <a href="https://packages.debian.org/unstable/devel/lintian">Lintian</a>
      è un programma che controlla se un pacchetto è conforme alla «policy».
      Gli sviluppatori dovrebbe usarlo prima di ogni caricamento.
      </dd>

    <dt><a href="$(DOC)/manuals/developers-reference/resources.html#experimental">Debian
    &lsquo;Experimental&rsquo;</a></dt>
      <dd>
      La distribuzione &lsquo;experimental&rsquo; è utilizzata come area d'appoggio per software
      particolarmente sperimentale. Installare pacchetti in <a
      href="https://packages.debian.org/experimental/">experimental</a> solo dopo aver capito
      come funziona &lsquo;unstable&rsquo;.
      </dd>

    <dt><a href="https://wiki.debian.org/HelpDebian">Wiki Debian</a></dt>
      <dd>
      Il Wiki di Debian con suggerimenti per sviluppatori e alrti contributori.
      </dd>
</dl>

<h2><a id="projects">Progetti: gruppi interni e progetti</a></h2>

<aside class="light">
  <span class="fas fa-folder-open fa-5x"></span>
</aside>

<ul>
<li><a href="website/">Pagine web Debian</a></li>
<li><a href="https://ftp-master.debian.org/">L'archivio Debian</a></li>
<li><a href="$(DOC)/ddp">Il progetto della documentazione Debian (DDP)</a></li>
<li><a href="https://qa.debian.org/">Il gruppo del controllo di qualità (QA) Debian</a></li>
<li><a href="$(HOME)/CD/">Immagini CD/DVD</a></li>
<li><a href="https://wiki.debian.org/Keysigning">Firma delle chiavi</a></li>
<li><a href="https://wiki.debian.org/Keysigning/Coordination">Coordinamento per la firma delle chiavi</a></li>
<li><a href="https://wiki.debian.org/DebianIPv6">Progetto Debian IPv6</a></li>
<li><a href="buildd/">La rete degli autobuilder</a> e i loro <a href="https://buildd.debian.org/">log</a></li>
<li><a href="$(HOME)/international/l10n/ddtp">Il progetto di traduzione delle descrizioni Debian (DDTP)</a></li>
<li><a href="debian-installer/">L'installare Debian</a></li>
<li><a href="debian-live/">Debian Live</a></li>
<li><a href="$(HOME)/women/">Debian Women</a></li>
<li><a href="$(HOME)/blends/">Debian Pure Blends</a></li>
</ul>

<h2><a id="miscellaneous">Collegamenti vari</a></h2>

<aside class="light">
  <span class="fas fa-bookmark fa-5x"></span>
</aside>

<ul>
<li><a href="https://debconf-video-team.pages.debian.net/videoplayer/">Registrazione</a> delle nostre conferenze</li>
<li><a href="passwordlessssh">Impostare SSH</a> in modo che non chieda una password</li>
<li>Come <a href="$(HOME)/MailingLists/HOWTO_start_list">richiedere una nuova lista di messaggi</a></li>
<li>Informazioni su come fare il <a href="$(HOME)/mirror/">mirror di Debian</a></li>
<li>Il <a href="https://qa.debian.org/data/bts/graphs/all.png">grafico di tutti i bug</a></li>
<li><a href="https://ftp-master.debian.org/new.html">Nuovi pacchetti</a> in attesa di essere inclusi in Debian (NEW Queue)</li>
<li><a href="https://packages.debian.org/unstable/main/newpkg">Nuovi pacchetti Debian</a> degli ultimi 7 giorni</li>
<li><a href="https://ftp-master.debian.org/removals.txt">Pacchetti rimossi da Debian</a></li>
</ul>

