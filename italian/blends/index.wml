#use wml::debian::template title="Debian Pure Blend" BARETITLE="true"
#use wml::debian::translation-check translation="6d2e0afb56e760364713c2cca2c9f6407c9a744f"

<p>
Debian Pure Blend è una soluzione pronta all'uso per gruppi di persone con esigenze specifiche.
Non solo fornisce raccolte (meta-pacchetti),
ma ne facilita l'installazione e la configurazione per lo scopo previsto dal sistema.
Le Blend coprono le necessità più diverse: bambini,
scienziati, videogiocatori, avvocati, personale medico, ipovedenti, ecc.
L'obiettivo comune di tutte le Blend è semplificare installazione, uso e amministrazione dei
computer per gli utenti di destinazione e permettere loro di contattare
le persone che realizzano o pacchettizzano il software che usano.
</p>

<p>Puoi trovare altre informazioni su Debian Pure Blend nel <a
href="https://blends.debian.org/blends/">Manuale di Pure Blend</a>.</p>

<ul class="toc">
<li><a href="#released">Pure Blend rilasciate</a></li>
<li><a href="#unreleased">Pure Blend future</a></li>
<li><a href="#development">Sviluppo</a></li>
</ul>

<div class="card" id="released">
<h2>Pure Blend rilasciate</h2>
<div>
<p>"Rilasciato" può avere significati diversi in Blend diverse. Nella maggior parte dei casi
significa che la Blend contiene dei meta-pacchetti oppure un installer che fanno parte di un
rilascio stabile di Debian. Blend può fornire il supporto d'installazione di una 
distribuzione derivata, oppure addirittura formarne la base. Per capire queste differenze 
è necessario leggere le pagine di ogni Blend.</p>

<table class="tabular" summary="">
<tbody>
 <tr>
  <th>Blend</th>
  <th>Descrizione</th>
  <th>Link</th>
 </tr>
#include "../../english/blends/released.data"
</tbody>
</table>
</div>
</div>

<div class="card" id="unreleased">
  <h2>Pure Blend future</h2>
  <div>
   <p>Queste blend sono ancora in fase di sviluppo e non ne è ancora stato fatto nessun rilascio
   stabile: possono però essere disponibili i rilasci <a
href="https://www.debian.org/releases/testing/">testing</a> o <a
href="https://www.debian.org/releases/unstable">unstable</a>.Alcune
blend in fase di sviluppo potrebbero ancora contenere del software Non Libero.</p>
  
<table class="tabular" summary="">
<tbody>
 <tr>
  <th>Blend</th>
  <th>Descrizione</th>
  <th>Link</th>
 </tr>
#include "../../english/blends/unreleased.data"
</tbody>
</table>
  </div>
 </div>
 
 <div class="card" id="development">
  <h2>Sviluppo</h2>
  <div>
   <p>Se sei interessato a collaborare allo sviluppo di Debian Pure
Blend puoi trovare delle informazioni su come collaborare nella <a
href="https://wiki.debian.org/DebianPureBlends">wiki</a>.
   </p>
  </div>
</div><!-- #main -->

