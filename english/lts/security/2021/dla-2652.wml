<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in Unbound, a validating,
recursive, caching DNS resolver, by security researchers of X41 D-SEC located
in Aachen, Germany. Integer overflows, assertion failures, an out-of-bound
write and an infinite loop vulnerability may lead to a denial-of-service or
have a negative impact on data confidentiality.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1.9.0-2+deb10u2~deb9u2.</p>

<p>We recommend that you upgrade your unbound1.9 packages.</p>

<p>For the detailed security status of unbound1.9 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/unbound1.9">https://security-tracker.debian.org/tracker/unbound1.9</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2652.data"
# $Id: $
