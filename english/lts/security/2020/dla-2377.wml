<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in qt4-x11, the legacy version
of the Qt toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15518">CVE-2018-15518</a>

    <p>Double-free or corruption in QXmlStreamReader
     during parsing of a specially crafted illegal XML document.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19869">CVE-2018-19869</a>

    <p>A malformed SVG image causes a segmentation fault.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19870">CVE-2018-19870</a>

    <p>A malformed GIF image causes a NULL pointer
     dereference in QGifHandler resulting in a segmentation fault.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19871">CVE-2018-19871</a>

    <p>Uncontrolled Resource Consumption in QTgaFile.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19872">CVE-2018-19872</a>

    <p>A malformed PPM image causes a crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19873">CVE-2018-19873</a>

    <p>QBmpHandler segfault on malformed BMP file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17507">CVE-2020-17507</a>

    <p>Buffer over-read in the XBM parser.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4:4.8.7+dfsg-11+deb9u1.</p>

<p>We recommend that you upgrade your qt4-x11 packages.</p>

<p>For the detailed security status of qt4-x11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qt4-x11">https://security-tracker.debian.org/tracker/qt4-x11</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2377.data"
# $Id: $
