#use wml::debian::template title="我们的理念：我们为什么要做，我们如何去做" MAINPAGE="true"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

#include "$(ENGLISHDIR)/releases/info"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#freedom">我们的使命：创建一个自由的操作系统</a></li>
    <li><a href="#how">我们的价值观：Debian 社区如何运作</a></li>
  </ul>
</div>

<h2><a id="freedom">我们的使命：创建一个自由的操作系统</a></h2>

<p>Debian 项目是一个由个人组成的组织，所有人
拥有一个共同目标：我们想要创建一个自由的操作系统，让所有人都能够自由获取。
这里，当我们使用自由（Free）这个词时，我们关心的不是金钱，而是
软件<em>自由</em>。</p>

<p style="text-align:center"><button type="button"><span class="fas fa-unlock-alt fa-2x"></span> <a href="free">我们对自由软件的定义</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://www.gnu.org/philosophy/free-sw">阅读自由软件基金会的声明</a></button></p>

<p>您可能想知道：是什么让人们愿意花费他们自己的时间去编写
软件，仔细地打包和维护它，然后再把它免费送人呢？好吧，
有很多原因，以下是其中的几个：</p>

<ul>
  <li>有些人只是喜欢帮助别人，而向自由软件项目作出贡献是达成这一愿望的很好的方式。</li>
  <li>许多开发者编写程序来学习更多关于计算机、不同的架构和编程语言的知识。</li>
  <li>有些开发者以作出贡献的方式对从他人那里获得的优秀的自由软件表示感谢。</li>
  <li>还有许多学术界人士开发自由软件来共享他们的研究成果。</li>
  <li>企业也帮助维护自由软件，这样他们就可以对软件的开发有发言权，或者能够更快地实现新功能。</li>
  <li>当然，大多数 Debian 开发者参与其中，只是因为觉得很有趣！</li>
</ul>

<p>尽管我们信仰自由软件，但在某些情况下，人们不得不在他们的机器上
安装非自由软件——不管他们愿不愿意。我们尊重这一点，并决定尽可能地
支持这部分用户。有越来越多的软件包
可以将非自由软件安装到 Debian 系统中。</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 我们投身于自由软件，并将我们的承诺化为一份正式的文档：我们的<a href="$(HOME)/social_contract">社群契约</a></p>
</aside>

<h2><a id="how">我们的价值观：Debian 社区如何运作</a></h2>

<p>Debian 项目在<a
href="$(DEVEL)/developers.loc">世界各地</a>有超过一千名活跃的<a
href="people">开发者和贡献者</a>。如此庞大的项目需要精心组织的<a
href="organization">组织架构</a>。所以，如果您想了解 Debian 项目是
如何运作的，以及 Debian 社区是否有规则和指导方针，请查看以下
的声明：</p>

<ul>
<li><a href="$(DEVEL)/constitution">Debian 宪章</a></li>
<li><a href="../social_contract">社群契约与自由软件指导方针</a></li>
<li><a href="diversity">多样性声明</a></li>
<li><a href="../code_of_conduct">行为准则</a></li>
<li><a href="../doc/developers-reference/">开发者参考</a></li>
<li><a href="../doc/debian-policy/">Debian 政策</a></li>
</ul>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><a href="$(DEVEL)/constitution">Debian 宪章</a>：<br>
          本文档描述了 Debian 项目的组织架构，并解释了作出正式决策的流程。</li>
        <li><a href="../social_contract">社群契约与自由软件指导方针</a>：<br>
          Debian 社群契约和作为其一部分的 Debian 自由软件指导方针（DFSG）描述了我们对自由软件和自由软件社区的承诺。</li>
        <li><a href="diversity">多样性声明</a>：<br>
           Debian 项目欢迎并鼓励所有人的参与，无论您如何认识自己，也无论他人如何看待您。</li>
      </ul>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><a href="../code_of_conduct">行为准则</a>：<br>
          我们为邮件列表和 IRC 频道等的参与者通过了一份行为准则。</li>
        <li><a href="../doc/developers-reference/">开发者参考</a>：<br>
          本文档提供了针对 Debian 开发者和维护者的推荐的工作流程和可获取的资源的概览。</li>
        <li><a href="../doc/debian-policy/">Debian 政策</a>：<br>
          本指南描述了 Debian 发行版的政策要求，例如，Debian 档案库的结构和内容、所有软件包被接受前必须满足的技术要求，等等。</li>
      </ul>
    </div>
  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-code fa-2x"></span>深入了解 Debian：<a href="$(DEVEL)/">开发者天地</a></button></p>
