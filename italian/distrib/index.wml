#use wml::debian::template title="Ottenere Debian"
#use wml::debian::translation-check translation="0691e0b35ed0aa5df10a4b47799051f77e519d25" maintainer="Giuseppe Sacco"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Debian è distribuita <a href="../intro/free">liberamente</a>
attraverso Internet ed è completamente scaricabile da uno qualsiasi dei
<a href="ftplist">mirror</a>.
Il <a href="../releases/stable/installmanual">manuale di installazione</a>
contiene informazioni dettagliate per l'installazione.
Le note di rilascio possono essere trovate <a
href="../releases/stable/releasenotes">qui</a>.</p>

<p>In questa pagina è indicato come installare Debian Stable. Chi è
interessato a Testing o Unstable, può visitare la <a
href="../releases/">pagine delle versioni</a>.</p>

<div class="line">
 <div class="item col50">
  <h2><a href="netinst">Scaricare un file immagine</a></h2>
  <p>A seconda delle connessione Internet di cui si dispone, 
  si può scaricare uno dei seguenti file:</p>
  <ul>
      <li>Un <a href="netinst"><strong>piccolo file immagine di
      installazione</strong></a>:
      è rapido da scaricare e va copiato su un supporto rimovibile.
      Per usare questo tipo di file immagine, si necessiterà di una
      macchina con accesso ad Internet.
      <ul class="quicklist downlist">
	  <li><a title="scarica l'installatore per PC 64-bit"
	         href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">netinst
		 iso 64-bit PC</a></li>
	  <li><a title="scarica l'installatore per i comuni PC 32-bit"
		 href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">netinst
		 iso 32-bit PC</a></li>
	</ul>
      </li>
      <li>Un <a href="../CD/"><strong>file immagine di installazione
      completo</strong></a>: più grande rispetto al precedente, contiene
      un maggior numero di pacchetti e rende quindi più semplice
      l'installazione di Debian su macchine prive di una connessione a
      Internet.
	<ul class="quicklist downlist">
	  <li><a title="scarica i torrent per il DVD di installazione per PC 64-bit"
	         href="<stable-images-url/>/amd64/bt-dvd/">torrent per PC 64-bit (DVD)</a></li>
	  <li><a title="scarica i torrent per il DVD di installazione per PC 32-bit"
		 href="<stable-images-url/>/i386/bt-dvd/">torrent per PC 32-bit (DVD)</a></li>
	  <li><a title="scarica i torrent per i CD di installazione per PC 64-bit"
	         href="<stable-images-url/>/amd64/bt-cd/">torrent per PC 64-bit (CD)</a></li>
	  <li><a title="scarica i torrent per i CD di installazione per PC 32-bit"
		 href="<stable-images-url/>/i386/bt-cd/">torrent per PC 32-bit (CD)</a></li>
	</ul>
      </li>
    </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="https://cloud.debian.org/images/cloud/">Usare un'immagine Debian in cloud</a></h2>
    <p>Una <a href="https://cloud.debian.org/images/cloud/"><strong>immagine cloud</strong></a>, realizzata dal gruppo cloud, può essere usata in:</p>
    <ul>
      <li>il proprio provider OpenStack, nei formati «qcow2» o «row».
        <ul class="quicklist downlist">
          <li>64-bit AMD/Intel (<a title="Immagine OpenStack per 64-bit AMD/Intel nel formato qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.qcow2">qcow2</a>, <a title="Immagine OpenStack per 64-bit AMD/Intel nel formato raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.raw">raw</a>)</li>
          <li>64-bit ARM (<a title="Immagine OpenStack per 64-bit ARM nel formato qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.qcow2">qcow2</a>, <a title="Immagine OpenStack per 64-bit ARM nel formato raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.raw">raw</a>)</li>
          <li>64-bit Little Endian PowerPC (<a title="Immagine OpenStack per 64-bit Little Endian PowerPC nel formato qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-ppc64el.qcow2">qcow2</a>, <a title="Immagine OpenStack per 64-bit Little Endian PowerPC nel formato raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-ppc64el.raw">raw</a>)</li>
        </ul>
      </li>
      <li>Amazon EC2, sia come immagine di macchina, sia tramite il marketplace di AWS.
        <ul class="quicklist downlist">
          <li><a title="Immagini di macchina Amazon" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Immagini di macchina Amazon</a></li>
          <li><a title="Marketplace AWS" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">Marketplace AWS</a></li>
        </ul>
      </li>
      <li>Microsoft Azure, nel marketplace di Microsoft
        <ul class="quicklist downlist">
          <li><a title="Debian 11 sul Marketplace Azure" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 ("Bullseye")</a></li>
          <li><a title="Debian 10 sul Marketplace Azure" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-10?tab=PlansAndPrice">Debian 10 ("Buster")</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>
<div class="line">
  <div class="item col50">
    <h2>È anche possibile <a href="../CD/vendors/">acquistare un set di
    CD o DVD da uno dei rivenditori di CD Debian</a></h2>

   <p>
      Molti di questi venditori ti permetteranno di acquistare la
      distribuzione per meno di 5 dollari USA (poco meno di 5 Euro), 
      più le spese di spedizione (consultare il sito web del venditore
      per verificare che effettui spedizioni internazionali).
      <br />
      In alcuni casi, i <a href="../doc/books">libri su Debian</a>
      vengono venduti con CD di installazione acclusi.
   </p>

   <p>Ecco alcuni vantaggi dei CD:</p>

   <ul>
     <li>l'installazione da CD è più semplice</li>
     <li>è possibile installare Debian su macchine prive di connessione a
     Internet</li>
     <li>si può installare Debian (su quante macchine si desidera)
      senza dover scaricarsi tutti i pacchetti</li>
     <li>il CD di installazione può essere usato come disco di ripristino
     su un sistema danneggiato.</li>
   </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="pre-installed">Comprare un computer con Debian gi&agrave;
    installata</a></h2>
   <p>Vi sono una serie di vantaggi da considerare legati a questo
   metodo:</p>
   <ul>
    <li>non si deve installare Debian</li>
    <li>l'installazione è già configurata per l'hardware</li>
    <li>il rivenditore può fornire supporto tecnico</li>
   </ul>
  </div>
  <div class="item col50 lastcol">
    <h2><a href="../CD/live/">Prova Debian live prima dell'installazione</a></h2>
    <p>
     È possibile provare Debian avviando un sistema live da un CD, un DVD
     o una chiave USB senza dover installare nessun file sul computer.
     Quando si è pronti, si potrà avviare l'installazione direttamente
     dal sistema live (a partire da Debian 10 Buster tramite il semplice
	 da usare <a href="https://calamares.io">Calamares</a>).
	 Questo metodo di installazione potrebbe essere
     quello adeguato qualora l'immagine live coincida con i propri
     requisiti di spazio, lingua e selezione dei pacchetti.
     Puoi consultare <a href="../CD/live#choose_live">ulteriori
     informazioni su questo metodo</a>.
    </p>
    <ul class="quicklist downlist">
      <li><a title="scarica i torrent per Debian Live per PC 64-bit"
	     href="<live-images-url/>/amd64/bt-hybrid/">torrent per Debian Live per PC 64-bit</a></li>
      <li><a title="scarica i torrent per Debian Live per PC 32-bit"
	     href="<live-images-url/>/i386/bt-hybrid/">torrent per Debian Live per PC 32-bit</a></li>
    </ul>
  </div>
</div>
   
# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Se sul proprio sistema è presente un qualsiasi hardware che
<strong>richiede il caricamento di firmware non-free</strong> insieme ai
driver del dispositivo, è possibile usare uno dei <a
href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/bullseye/current/">tarball
con i pacchetti firmware più comuni</a> oppure scaricare
un'immagine <strong>non ufficiale</strong> che contiene i firmware
<strong>non-free</strong>. Le istruzioni su come usare questi tarball e
le informazioni su come caricare il firmware durante l'installazione
possono essere trovate nella <a href="../amd64/ch06s04">guida
all'installazione</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">immagini
non ufficiali di <q>stable</q> con firmware incluso</a>
</p>
</div>
