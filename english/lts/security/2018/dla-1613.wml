<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Security experts at Tencent’s Blade security team have discovered a
critical vulnerability in SQLite database software (nicknamed <q>Magellan</q>).</p>

<p>The <q>Magellan</q> remote code execution vulnerability has now been fixed by
adding extra defenses against strategically corrupt databases to fts3/4.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.8.7.1-1+deb8u3.</p>

<p>We recommend that you upgrade your sqlite3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1613.data"
# $Id: $
